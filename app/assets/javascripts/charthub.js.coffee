$ ->
  # アラート表示をクローズしたときに標準では要素をremoveしてしまうのでhideするよう修正。
  $('#notify_message').bind('close', ->
    $('#notify_message').hide()
    return false
  )

# 文字列の幅測定用の関数
@getStrWidth = (str) ->
  e = $("#ruler")
  if e != undefined
   width = e.text(str).get(0).offsetWidth;
   e.empty()
   return width

@showMsgTip = (str, typ) ->
  str = str || ''
  switch typ
    when 'info','success','error'
      cls = 'alert-' + typ
    else
      cls = ''
  $('div#notify_message').hide()
  #$('div#notify_message_text').text(str)
  $('div#notify_message_text').html(str)
  $('div#notify_message').removeClass()
  $('div#notify_message').addClass('alert alert-block')
  $('div#notify_message').addClass(cls)
  #$('div#notify_message').stop().animate({'marginTop':'0'},750).delay(1500).slideUp(300)
  #$('div#notify_message').show()
  $('div#notify_message').slideDown()

@showLoadingImg = (container_id, ldimg_id) ->
  $('#' + ldimg_id).remove()
  $('<div id="' + ldimg_id + '" style="text-align:center;"><img src="/assets/loading-24.gif" />Now loading...</div>').appendTo('#' + container_id)
  
