$(document).ready(function(){

  $('#series_select').change(function(){
    var series_id = $(this).val();
    $.ajax({
      type: "GET",
      url: "/series/" + series_id + "/load",
      success: function(data){
        $('#reference_value').empty();
        for(var i = 1; i < data['data'][0].length; i++){
          $('#reference_value').append($('<option>').attr({ value: data['data'][0][i] }).text(data['data'][0][i]+':'+data['data'][1][i]));
        };
      }
    });
  });

});
