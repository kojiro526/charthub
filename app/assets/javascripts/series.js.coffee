# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$ ->
  $("#edit_series_dialog").hide()
  $("#series_table_area").attr("class", "span9")
  #$("#edit_series_button").show()
  $("#close_series_button").hide()
  $("#preview_values_dialog").hide()

  $('#preview_values').click( ->

    f = $('#edit_series_form')
    $.ajax({
      type: "POST",
      url: '/series/preview',
      dataType: 'json',
      data: f.serialize(),
      beforeSend: (xhr) ->
        # モーダルオーバレイを表示
        #$('body').append('<div id="modal-overlay"></div>')
        #$('#modal-overlay').fadeIn('slow')

        # オーバーレイをクリックするとプレヴューを解除。
        #$('#modal-overlay').click( ->
        #  $('#modal-overlay').remove()
        #  $("#preview_values_dialog").dialog("destroy")
        #)

        # プレビューダイアログを表示
        # ダイアログに残っている要素を空にする。
        $('div#preview_values_dialog_content').empty()
        $("#preview_values_dialog").dialog({
          modal: true
          draggable: true
          width: 500
          height: $(window).height() - 20
        })

        $('<div id="ldimg_preview_values" style="text-align:center;"><img src="/assets/loading-24.gif" />Now loading...</div>').appendTo("#preview_values_dialog_content")

      success: (data) ->
        $('div#ldimg_preview_values').remove()
        $('div#preview_values_dialog_content').append(data.html)
        #showMsgTip("SUCCESS","success")
      error: (xhr, status, error) ->
        $('div#ldimg_preview_values').remove()
        showMsgTip(xhr.responseText,"error")
    })
  )

  # プレビュー後に値の更新を確定する。
  $('#commit_values').click( ->
    f = $('#edit_series_form')
    f.submit()
  )

@showEditSeriesDialog = ->
  $("#edit_series_dialog").show()
  $("#series_table_area").attr("class", "span6")
  #$("#edit_series_button").hide()
  $("#close_series_button").show()
  
@hideEditSeriesDialog = ->
  $("#edit_series_dialog").hide()
  $("#series_table_area").attr("class", "span9")
  #$("#edit_series_button").show()
  $("#close_series_button").hide()
