# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$ ->
  $('#add_series').click ->
    add_series_to_collection()

add_series_to_collection = ->
  selected_value = $('#all_series').val()
  selected_text  = $('#all_series option:selected').text()
  $('#selected_series').append($('<option>').html(selected_text).val(selected_value))
  reflesh_selected_series()

reflesh_selected_series = ->
  $('#selected_series_hidden').empty()
  $('#selected_series').children('option').each( ->
    $('#selected_series_hidden').append($('<input type="hidden" name="selected_series_hidden[]">').val($(this).val()))
  )
