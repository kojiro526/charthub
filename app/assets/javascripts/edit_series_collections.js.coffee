$ ->
  $('#series_select_area').hide()
  # 「閉じる」ボタンクリック時
  $('a.close').click( ->
    $(this).parent().hide()
  )

  # 更新時の動作
  $('#edit_collection_form')
  .bind("ajax:success", (e, data, status, xhr)->
    showMsgTip(data['notice'], 'success')
    #$('.notice > span').text(data['notice'])
    #$('.notice').show()
  )

  $('#close_selector').click( ->
    $('#series_select_area').hide()
    $('#edit_collections').show()
    $('#ldimg_series').remove()
    $('#ldimg_selected_series').remove()
  )

  # 「編集」ボタンクリック時
  $('#edit_collections').click( ->
    #$('.notice').hide()
    login = $('#login_name').val()
#    series_collection_id = $('#series_collection_id').val()

    $("#series_alias").hide()
    $("#series_calculator_properties").hide()
    $("#source_series").empty()
    $("#selected_series").empty()

    # 系列を全て取得
    getSourceSeries(login, '')

    # 当該系列コレクションの系列を取得
    series_type = $('#series_type').val() 
    switch series_type
      when 'series_collection'
        series_collection_id = $('#series_collection_id').val()
        $.ajax({
          type: "GET",
          #url: "/series_collections/" + series_collection_id  + "/collection.json",
          url: "/tables/" + series_collection_id  + "/collection.json",
          beforeSend: (xhr) ->
            $('<div id="ldimg_selected_series" style="text-align:center;"><img src="/assets/loading-24.gif" />Now loading...</div>').appendTo("#selected_series_area")
          success: (data) ->
            $('#ldimg_selected_series').remove()
            for row in data
              switch row.row_type
                when 'series'
                  switch row.category_axis_type
                    when 'none'
                      axis_type = 'TXT'
                      axis_type_label = 'テキスト'
                    when 'integer'
                      axis_type = 'INT'
                      axis_type_label = '数値'
                    when 'date_y'
                      axis_type = 'Y'
                      axis_type_label = '年'
                    when 'date_ym'
                      axis_type = 'YM'
                      axis_type_label = '年月'
                    when 'date_ymd'
                      axis_type = 'YMD'
                      axis_type_label = '年月日'
                  tmpl_conf = {
                    no: row.sequence+1
                    series_order_id: row.series_order_id
                    series_id: row.series_id
                    name: row.name
                    alias: row.alias,
                    axis_type: axis_type,
                    axis_type_label: axis_type_label
                  }
                  $('#selected_series_template').tmpl(tmpl_conf).appendTo("#selected_series")
                when 'series_calculator'
                  tmpl_conf = {
                    no: row.sequence+1
                    series_calculator_id: row.series_calculator_id
                    name: row.name
                    expression: row.expression
                  }
                  $('#selected_series_template_for_calculator').tmpl(tmpl_conf).appendTo("#selected_series")
        })

    $('#series_select_area').show('blind','',1000)
    $(this).hide()
  )

  # 系列の検索
  $('#search_series').click( ->
    login = $('#login_name').val()
    search_str = $('#search_query').val()
    
    getSourceSeries(login, search_str)
  )

  # 選択された系列を選択済みのリストにセット
  $('#add_series').click( ->
    series_id = $('#selected_series_id').val()
    if series_id? && series_id != ""

      series_type = $('#series_type').val()

      # 系列の情報を取得してリストにセット
      $.ajax({
        type: "GET",
        url: "/series/" + series_id + ".json"
        success: (data) ->

          switch series_type
            when 'series_collection'
              switch data.category_axis_type
                when 'none'
                  axis_type = 'TXT'
                  axis_type_label = 'テキスト'
                when 'integer'
                  axis_type = 'INT'
                  axis_type_label = '数値'
                when 'date_y'
                  axis_type = 'Y'
                  axis_type_label = '年'
                when 'date_ym'
                  axis_type = 'YM'
                  axis_type_label = '年月'
                when 'date_ymd'
                  axis_type = 'YMD'
                  axis_type_label = '年月日'
              tmpl_conf = {
                row_type: 'series'
                series_order_id: ''
                series_id: data.id
                name: data.name,
                axis_type: axis_type,
                axis_type_label: axis_type_label
              }
              $('#selected_series_template').tmpl(tmpl_conf).appendTo("#selected_series")
      })
      
    else
      alert('系列を選択してください。')
  )

  # 計算系列を追加
  $('#add_expression').click( ->
    tmpl_conf = {
      row_type: 'series_calculator'
      series_calculator_id: ''
      name: 'expression'
      expression: ''
    }
    $('#selected_series_template_for_calculator').tmpl(tmpl_conf).appendTo("#selected_series")
  )

  # 系列の別名をセット
  $('#update_alias').click( ->
    alias = $('#series_name_alias').val()
    #tr = $('#selected_series').children('tr.selected:first')
    tr = $('#selected_series tr.selected:first')
    tr.children('td.alias:first').text(alias)
    tr.find('input.alias:first').val(alias)
  )

  # 式をセット
  $('#update_calculator').click( ->
    name = $('#series_calculator_name').val()
    expression = $('#series_calculator_expression').val()
    tr = $('#selected_series tr.selected:first')
    tr.children('td.name:first').text(name)
    tr.children('td.expression:first').text(expression)
    tr.find('input.name:first').val(name)
    tr.find('input.expression:first').val(expression)
  )

  # 選択されている系列を上へ移動
  $('#move_up').click( ->
    # 選択されている列を取得
    current = $('table#selected_series').find('tr.selected:first')
    if $(current).size() == 0
      alert('系列を選択してください。')
      return
    target = $(current).prev()
    $(current).insertBefore(target)
  )

  # 選択されている系列を下へ移動
  $('#move_down').click( ->
    # 選択されている列を取得
    current = $('table#selected_series').find('tr.selected:first')
    target = $(current).next()
    $(current).insertAfter(target)
  )

@selectSeriesRow = (t)->
  $(t).siblings('tr').each( ->
    $(@).removeClass('selected')
  )
  $(t).addClass('selected')
  $('#selected_series_id').val($(t).attr('id'))

@selectCollectionRow = (t)->
  $(t).siblings('tr').each( ->
    $(@).removeClass('selected')
  )
  $(t).addClass('selected')

  alias_name = $(t).find('td.alias:first').text()
  $('#series_name_alias').val(alias_name)

  $('#series_calculator_properties').hide()
  $('#series_alias').show()

@selectCollectionRowCulc = (t)->
  $(t).siblings('tr').each( ->
    $(@).removeClass('selected')
  )
  $(t).addClass('selected')

  name = $(t).find('td.name:first').text()
  $('#series_calculator_name').val(name)

  expression = $(t).find('td.expression:first').text()
  $('#series_calculator_expression').val(expression)

  $('#series_alias').hide()
  $('#series_calculator_properties').show()

@removeSelectedSeries = (t)->
  if confirm("削除しますか？")
    $(t).parent().parent("tr.select_table_row").remove()

@getSourceSeries = (login, str) ->
  $('#source_series').empty()
  data_obj = {}
  unless str == ''
    data_obj.keyword = str

  $.ajax({
    type: "GET",
    url: "/" + login + "/series.json",
    data: data_obj
    beforeSend: (xhr) ->
      $('<div id="ldimg_series" style="text-align:center;"><img src="/assets/loading-24.gif" />Now loading...</div>').appendTo("#source_series_area")
    success: (data) ->
      $('#ldimg_series').remove()
      for series in data
        switch series.category_axis_type
          when 'none'
            axis_type = 'TXT'
            axis_type_label = 'テキスト'
          when 'integer'
            axis_type = 'INT'
            axis_type_label = '数値'
          when 'date_y'
            axis_type = 'Y'
            axis_type_label = '年'
          when 'date_ym'
            axis_type = 'YM'
            axis_type_label = '年月'
          when 'date_ymd'
            axis_type = 'YMD'
            axis_type_label = '年月日'
        tmpl_conf = {series_id: series.id, name: series.name, axis_type: axis_type, axis_type_label: axis_type_label}
        $('#source_series_template').tmpl(tmpl_conf).appendTo("#source_series")
      $('span#count_series').text(data.length)
    })
