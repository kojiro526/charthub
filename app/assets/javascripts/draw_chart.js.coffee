@drawChartInit = (canvas_id, chart_config, chart_data, chart_memos) ->
  drawChart(canvas_id, chart_config, chart_data, chart_memos)

  canvas = document.getElementById(canvas_id)
  canvas.addEventListener('click', editPoss, false)

  canvas.gs = new Object
  canvas.gs.x_form_id = ''
  canvas.gs.y_form_id = ''
  canvas.gs.set_poss = false
  canvas.gs.load_path = ''


@drawChart = (canvas_id, chart_config, chart_data, chart_memos) ->
  chartdata = new Object
  chartdata.config = chart_config
  chartdata.data = chart_data
  ccchart.init(canvas_id, chartdata)

  unless chart_memos == undefined
    for memo in chart_memos
      ccchart.memo(memo)

@editPoss = (e) ->
  if this.gs.set_poss == true 
    rect = e.target.getBoundingClientRect()
    x = e.clientX - Math.floor(rect.left)
    y = e.clientY - Math.floor(rect.top)

    if this.gs.x_form_id != ''
      $('#' + this.gs.x_form_id).val(x)

    if this.gs.y_form_id != ''
      $('#' + this.gs.y_form_id).val(y)

    unless this.gs.load_path is null || this.gs.load_path == '' || this.gs.load_path == undefined
      drawChartByEdit('graph-form', this.gs.load_path)

  e.stopPropagation()
      


