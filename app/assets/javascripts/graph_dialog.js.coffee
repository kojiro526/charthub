$ ->
  #ダイアログを初期化
  $("#graph_dialog_area").hide()
  $("#docking_dialog").hide()
  $("#popup_status").val('off')

  # 編集ボタン
  $("#edit_graph").click( ->
    graph_id = $("#graph_id_for_graph_dialog").val()
    location.href = "/graphs/" + graph_id + "/edit"
  )

  # ポップアップボタン
  $("#popup_dialog").click( ->
    width = $("canvas#graph_canvas").width
    $("#graph_dialog").dialog({
      modal: false
      width: width
      open: (event, ui) ->
             $(".ui-dialog-titlebar-close").hide()
    })
    $("#graph_dialog_area").hide()
    $("#popup_status").val('on')
    $("#popup_dialog").hide()
    $("#docking_dialog").show()
  )

  $("#docking_dialog").click( ->
    $("#popup_dialog").show()
    $("#docking_dialog").hide()
    $("#popup_status").val('off')
    $("#graph_dialog").dialog("destroy")
    $("#graph_dialog_area").show()
  )

  # 閉じるボタン
  $("#close_graph_dialog").click( ->
    if $("#popup_status").val() is 'on'
      $("#popup_dialog").show()
      $("#docking_dialog").hide()
      $("#graph_dialog").dialog("destroy")
      $("#graph_dialog_area").hide()
      $("#popup_status").val('off')
    else
      $("#graph_dialog_area").hide()
  )

  # 画像に変換ボタン
  $("#download_as_image").click( ->
    data = ccchart.toData({"canvas":graph_canvas, "image/png"})
    win = window.open()
    win.document.write('<img src="' + data + '">')
  )

  # 表示ボタン
  $("#show_graph").click( ->
    graph_id = $("#graph_id_for_graph_dialog").val()
    graph_owner = $("#graph_owner").val()
    location.href = "/" + graph_owner + "/graphs/" + graph_id
  )

  # オートリサイズボタン
  $("#graph_resizable").click( ->
    $("#graph_100p").toggleClass('disabled')
    $("#graph_100p").removeAttr('disabled')
    $("#graph_resizable").toggleClass('disabled')
    $("#graph_resizable").attr('disabled', 'disabled')

    canvas = document.getElementById("graph_canvas")
    $('#graph_canvas').css('width','100%')
    $('#graph_canvas').css('max-width', canvas.width+'px')
    $('#graph_canvas').css('height','auto')
  )

  # 100%表示ボタン
  $("#graph_100p").click( ->
    $("#graph_100p").toggleClass('disabled')
    $("#graph_100p").attr('disabled', 'disabled')
    $("#graph_resizable").toggleClass('disabled')
    $("#graph_resizable").removeAttr('disabled')

    canvas = document.getElementById("graph_canvas")
    $('#graph_canvas').css('width', canvas.width+'px')
    $('#graph_canvas').css('max-width', '')
    $('#graph_canvas').css('height', canvas.height+'px')
  )

  # サムネ入り一覧の開閉
  $('#child_graphs_toggle > i.icon-chevron-down').hide()
  $('#child_graphs_thumbs').hide()
  $('div#child_graphs_headline').click( ->
    $('i.toggle-chevron').toggle()
    $('#child_graphs_thumbs').toggle()
  )

  # サムネイルクリック時の動作
  $('.graph_thumb')
  .bind("ajax:beforeSend", (xhra) ->
    $('#graph_canvas_container').hide()
    $('canvas#graph_canvas').hide()
    $("#graph_dialog").show()
    $("#graph_dialog_area").show('blind','',1000) 
    showLoadingImg('graph_canvas_container', 'ldimg_graph_canvas')
    $('#graph_canvas_container').show()
  )
  .bind("ajax:success", (e, data, status, xhr) ->

    $('#graph_dialog').fontSpy({
      callback: $.noop
    })

    $('#ldimg_graph_canvas').remove()
    $('canvas#graph_canvas').show()

    $('#graph_id_for_graph_dialog').val(data['graph_id'])

    chartdata = {
      "config": data['config'],
      "data": data['data']
    }

    graph_owner = $("#graph_owner").val()
    $('#graph_url').val(absolutePath('/' + graph_owner + '/graphs/' + data['graph_id']))

    #$("#graph_dialog").show()
    #$("#graph_dialog_area").show('blind','',1000)
    ccchart.init('graph_canvas', chartdata)
    for memo in data['memos']
      ccchart.memo(memo)

    # デフォルトでキャンバスを画面にフィットさせるための設定。
    $('#graph_canvas').css('width','100%')
    $('#graph_canvas').css('max-width', data['config'].width+'px')
    $('#graph_canvas').css('height','auto')
  )

@absolutePath = (path) ->
  e = document.createElement('span')
  e.innerHTML = '<a href="' + path + '" />'
  return e.firstChild.href
