$ ->

  #ダイアログを初期化
  $("div#graph_dialog").hide()

  # テスト
  $(".reload_chart")
  .bind("ajax:success", (e, data, status, xhr) ->

    chartdata = {
      "config": data['config'],
      "data": data['data']
    }

    $("#graph_dialog").show()
    ccchart.init('graph_canvas', chartdata)
  )
