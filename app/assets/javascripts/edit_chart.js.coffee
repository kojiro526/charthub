$ ->
  $('#add_new_memo').click( ->
    addNewMemo()
    ###
    $.ajax({
      type: "GET",
      url: '/graphs/new_memo',
      success: (data) ->
        $("#memo_forms").append(data.html)
    })
    ###
  )

  $('div.series_option_form').hide()

  $('#all_series_options_open').click( ->
    $('div.series_option_form').show()
  )

  $('#all_series_options_close').click( ->
    $('div.series_option_form').hide()
  )

  # ヘッダーのクリックでフォームを開閉
  $('div.series_option_header').click( ->
    $(this).next("div.series_option_form").toggle()
  )

  # 系列オプションで色を設定した場合にプレビューを変更。
  $('input.series_color').change( ->
    color_tmp = $(this).val()
    $(this).parents('li.series_option').each( ->
      $(this).find('div.series_color_prev').css('background-color', color_tmp)
    )
  )

  # 系列選択のチェックボックスをクリックした際にヘッダーのクリックが発火しないように。
  $('input[type=checkbox].series_op_selected_checkbox').click( (e)->
    e.stopPropagation()
  )

@addNewMemo = ->

  next_count = $('#memo_forms').find('.ccchart_memo_form').size()
  $.ajax({
    type: "GET",
    url: '/graphs/new_memo',
    data: {counter: next_count},
    success: (data) ->
      $("#memo_forms").append(data.html)
  })


# フォームの入力値の設定でチャートを更新する。
# 系列のデータはAjaxでロードするが、将来的には
# HTML内に値を持たせてサーバへのリクエスト無しに
# 更新できるようにする。
# TODO: 2014-12-30 サーバ側でコンフィグを生成することに
#       したのでこの関数自体不要。
###
@editChart = (series_type, series_id) ->
  switch series_type
    when "series"
      url_tmp = "/series/" + series_id + "/load"
    when "series_collections"
      url_tmp = "/tables/" + series_id + "/load"
    when "graphs"
      url_tmp = "/graphs/" + series_id + "/load"

  $.ajax({
    type: "GET",
    url: url_tmp,
    success: (data) ->
      form = $('#graph-form') 
      # フォームの値をパラメータ化して取得
      $(form.serializeArray()).each((i, v) ->

        # パラメータ名を取得
        if v.name is "chart_area_select"
          # graph[xxx]以外の形式のパラメータを処理
          lcc_name = "chartAreaSelect"
        else
          # パラメータ名がgraph[xxx]の時は[]内を取り出す。
          # UPD 2014-09-14 graph[aaa][bbb]の場合もあるので最短一致に修正。
          cname = v.name.match(/graph\[(.*?)\]/)
          #v.name.match(/graph\[(.*?)\]/)
          #cname = RegExp.$1
          if cname != null && cname[1] != null
            # パラメータ名をLowerCammelCaseに変換
            # ex) x_color -> xColor
            lcc_name = toLCC(cname[1])
          else
            return true; # continueの代わり

        # 必要に応じてパラメータ名・値を変換
        hash_tmp = new Object();
        switch lcc_name
          when "chartType"
            lcc_name = "type"
            hash_tmp = 'type': v.value
          when "bgGradient"
            # 背景グラディエーション
            subkey = v.name.match(/graph\[bg_gradient\]\[(.*?)\]/)
            switch subkey[1]
              when 'direction'
                hash_tmp[lcc_name] = {direction: v.value}
              when 'from'
                hash_tmp[lcc_name] = {from: v.value}
              when 'to'
                hash_tmp[lcc_name] = {to: v.value}
          when "titleFont", "subTitleFont", "xScaleFont", "yScaleFont", "hanreiFont", "unitFont"
            re = new RegExp("graph\\[" + cname[1] + "\\]\\[(.*?)\\]")
            subkey = v.name.match(re)
            switch subkey[1]
              when 'alpha'
                hash_tmp[lcc_name] = {alpha: v.value}
              when 'size'
                hash_tmp[lcc_name] = {size: v.value}
              when 'font'
                hash_tmp[lcc_name] = {font: v.value}
          when "paddingTop","paddingBottom","paddingLeft","paddingRight","maxY","minY","maxX","minX","hanreiLineHeight","bg"
            # nullの場合は設定そのものを加えない。
            unless v.value is null || v.value is ""
              hash_tmp[lcc_name] = v.value
          when "chartAreaSelect"
            switch v.value
              when "all"
                hash_tmp =
                  "onlyChart": "no"
                  "onlyChartWidthTitle": "no"
              when "only_chart"
                hash_tmp =
                  "onlyChart": "yes"
                  "onlyChartWidthTitle": "no"
              when "chart_and_title"
                hash_tmp =
                  "onlyChart": "no"
                  "onlyChartWidthTitle": "yes"
          when "useVal","percentVal","useToolTip", "yScalePercent", "useHanrei", "changeRC"
            hash_tmp[lcc_name] = convBoolean(v.value)
          else
            hash_tmp[lcc_name] = v.value

        # キーと値をセット
        re = new RegExp("^[+-]*[0-9]+$")
        for key, value of hash_tmp
          if typeof(value) is "string" && value.match(re)
            # 数値が文字列のままでは上手く動かないのでキャスト
            data['config'][key] = parseInt(value)
          else
            if isObject(value)
              # サブキーの場合は連想配列になっているため、別関数で処理
              data['config'][key] = setSubKey(data['config'][key], value)
            else if typeof(value) is "string"
              data['config'][key] = value
            else
      )  

      unless data['config']['bg'] is undefined || data['config']['bg'] is null || data['config']['bg'] is ""
        delete data['config']['bgGradient']

      data['config']['titleFont'] = concatFontProp(data['config']['titleFont'])
      data['config']['subTitleFont'] = concatFontProp(data['config']['subTitleFont'])
      data['config']['xScaleFont'] = concatFontProp(data['config']['xScaleFont'])
      data['config']['yScaleFont'] = concatFontProp(data['config']['yScaleFont'])
      data['config']['hanreiFont'] = concatFontProp(data['config']['hanreiFont'])
      data['config']['unitFont'] = concatFontProp(data['config']['unitFont'])

      chartdata = {
        "config": data['config'],
        "data": series_data
      }
      ccchart.init('graph_canvas', chartdata)

  })
###

toLCC = (cname) ->
  cname_splited = cname.split("_")
  cc = ""
  for i in [0..cname_splited.length-1]
    if i is 0
      cc = cname_splited[i]
    else
      for j in [0..cname_splited[i].length-1]
        if j is 0
          cc += cname_splited[i].charAt(j).toUpperCase()
        else
          cc += cname_splited[i].charAt(j)
    
  return cc

convBoolean = (val) ->
  if val is "1"
    "yes"
  else
    "no"

setSubKey = (obj, v) ->
  if obj is undefined || obj is null || !isObject(obj)
    res = new Object
    for key,value of v
      res[key] = value
    return res
  else
    for key,value of v
      obj[key] = value
    return obj

# 連想配列かどうかを判定する。
isObject = (obj) ->
  return (obj instanceof Object && !(obj instanceof Array)) ? true : false

# 削除した関数内でしか使われていないので削除。
###
concatFontProp = (obj) ->
  #return obj['alpha'] + ' ' + obj['size'] + 'px ' + '"' + obj['font'] + '"'
  return '100 ' + obj['size'] + 'px ' + obj['font']
###

@editChartConf = (series_type, series_id) ->
  drawChartByEdit('graph-form', getEditLoadPath(series_type, series_id))

@drawChartByEdit = (form_id, load_path) ->

  form = $('#graph-form') 

  $.ajax({
    type: "PUT",
    url: load_path,
    data: form.serialize(),
    success: (data) ->
      $('#graph_dialog').fontSpy({
        callback: $.noop
      } )

      # HTMLタグ内に保存された系列の値データを取得する。
      series_data = $('div#chart_data_tmp').attr('data-chart-tmp')
      series_data = JSON.parse(series_data)

      if data['data'] == null
        # サーバから帰ってきたデータに系列の値データが含まれていなければ一時保存されたデータを使う。
        drawChart('graph_canvas', data['config'], series_data, data['memos'])
      else
        drawChart('graph_canvas', data['config'], data['data'], data['memos'])
        # HTMLタグ内に系列の値データを保存する。
        $('div#chart_data_tmp').attr('data-chart-tmp', JSON.stringify(data['data']))

      # 系列の選択状態を一時記憶用のフィールドにセット
      $('ul.series_options').find('div.series_option_header').each( ->
        if $(this).find('input.series_op_selected_checkbox').prop('checked') == true
          selected_tmp = "true"
        else
          selected_tmp = "false"
        $(this).children('input.series_op_selected_reg').val(selected_tmp)
      )

    error: (e, data, xhr) ->
      showMsgTip("ERROR","error")
  })

@deleteMemo = (t) ->
  fg = confirm('Are you sure?')
  if fg 
    $(t).parent('div.ccchart_memo_form').remove()

@getPossTest = (e) ->
  canvas = document.getElementById('graph_canvas')
  rect = e.target.getBoundingClientRect()
  #x = e.clientX - canvas.offsetLeft
  #y = e.clientY - canvas.offsetTop
  x = e.clientX - Math.floor(rect.left)
  y = e.clientY - Math.floor(rect.top)
  alert('e.clientX:' + e.clientX + ' canvas.offsetLeft:' + canvas.offsetLeft + ' x:'+x+' e.clientY:' + e.clientY + ' canvas.offsetTop:' + canvas.offsetTop + ' y:'+y)

# メモの座標をcanvas上でクリックした位置に設定する。
@getPossAndRedraw = (t, num) ->
  # ボタンの状態設定
  $('button.btn_getposs').removeAttr('disabled')
  $(t).attr('disabled', 'disabled')

  canvas = document.getElementById('graph_canvas')
  canvas.gs.x_form_id = 'left_' + num
  canvas.gs.y_form_id = 'top_' + num
  canvas.gs.load_path = getEditLoadPath($('#series_type').val(), $('#series_id').val())
  canvas.gs.set_poss = true

  # モーダルオーバレイを表示
  $('body').append('<div id="modal-overlay"></div>')
  $('#modal-overlay').fadeIn('slow')

  # オーバーレイをクリックすると位置取得モードを解除。
  $('#modal-overlay').click( ->
    $('#modal-overlay').remove()
    $("#graph_dialog").dialog("destroy")
    $("#graph_dialog_area").show()
    escapeSetPoss()
  )

  # canvasをダイアログとして前面に表示
  width = $("canvas#graph_canvas").width
  $("#graph_dialog").dialog({
    modal: false
    draggable: false
    width: width
    open: (event, ui) ->
           $(".ui-dialog-titlebar").hide()
  })
  $("#graph_dialog_area").hide()

# グラフ編集時に設定を読み込むためのパスを生成する。
@getEditLoadPath = (series_type, series_id) ->
  switch series_type
    when "series"
      url_tmp = "/graphs/config_test_new"
    when "series_collections"
      url_tmp = "/graphs/config_test_new"
    when "graphs"
      url_tmp = "/graphs/" + series_id + "/config_test_edit"

  return url_tmp

@escapeSetPoss = ->
  $('button.btn_getposs').removeAttr('disabled')

  canvas = document.getElementById('graph_canvas')
  canvas.gs.x_form_id = ''
  canvas.gs.y_form_id = ''
  canvas.gs.load_path = ''
  canvas.gs.set_poss = false
