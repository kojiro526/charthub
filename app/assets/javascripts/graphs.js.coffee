# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$ ->

  # オートリサイズボタン
  $("#graph_resizable").click( ->
    $("#graph_100p").toggleClass('disabled')
    $("#graph_100p").removeAttr('disabled')
    $("#graph_resizable").toggleClass('disabled')
    $("#graph_resizable").attr('disabled', 'disabled')

    canvas = document.getElementById("graph_canvas")
    $('#graph_canvas').css('width','100%')
    $('#graph_canvas').css('max-width', canvas.width+'px')
    $('#graph_canvas').css('height','auto')
  )

  # 100%表示ボタン
  $("#graph_100p").click( ->
    $("#graph_100p").toggleClass('disabled')
    $("#graph_100p").attr('disabled', 'disabled')
    $("#graph_resizable").toggleClass('disabled')
    $("#graph_resizable").removeAttr('disabled')

    canvas = document.getElementById("graph_canvas")
    $('#graph_canvas').css('width', canvas.width+'px')
    $('#graph_canvas').css('max-width', '')
    $('#graph_canvas').css('height', canvas.height+'px')
  )

  # 全て選択ボタン
  $("#all_values_selected").click( ->
    $("#choice_table_area").find("input[type='checkbox']").prop("checked",true)
   )

  # 選択解除ボタン
  $("#all_values_unselected").click( ->
    $("#choice_table_area").find("input[type='checkbox']").prop("checked",false)
   )

  $("#graph-form").submit ->
    series_type = $("#series_type").val()

    if $("#graph_id").val() == ""
      id_tmp = $("#series_id").val()
      #editChart(series_type, id_tmp) #画像化する前にcanvasを更新する。
      editChartConf(series_type, id_tmp)
    else
      id_tmp = $("#graph_id").val()
      #editChart('graphs', id_tmp) #画像化する前にcanvasを更新する。
      editChartConf('graphs', id_tmp)

    canvasToImage()

@canvasToImage = () ->
  data = ccchart.toData({"canvas":graph_canvas, "image/png"})
  $("#graph_image").val(data)

