$ ->
  $("#edit_desc_form")
  .bind("ajax:success", (e, data, status, xhr) ->

    series_type = $('#series_type_for_desc').val()
    switch series_type
      when 'series'
        url_tmp = '/series/' + data.id + '/desc'
      when 'series_collections'
        #url_tmp = '/series_collections/' + data.id + '/desc'
        url_tmp = '/tables/' + data.id + '/desc'
  
    $.ajax({
      type: "GET",
      url: url_tmp,
      success: (data) ->
        $('p#series_desc_text').empty()
        $('p#series_desc_text').html(data)
    })
    
    $('#series_desc_update_form').hide()
    $('#series_desc').show()
  )

  $('#series_desc_edit_button').click ->
    $('#series_desc_update_form').show()
    $('#series_desc').hide()

  $('#edit_description_cancel').click ->
    $('#series_desc_update_form').hide()
    $('#series_desc').show()
