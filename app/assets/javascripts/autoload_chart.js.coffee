$ ->

  series_id = $("#series_id").val()
  graph_id  = $("#graph_id").val()
  series_type = $("#series_type").val()
  action_type = $("#action_type").val()
  switch action_type
    when "new"
      switch series_type
        when "series"
          url_tmp = "/series/" + series_id + "/load"
        when "series_collections"
          url_tmp = "/tables/" + series_id + "/load"
    when "edit"
      url_tmp = "/graphs/" + graph_id + "/load"
  
  $.ajax({
    type: "GET",
    url: url_tmp,
    beforeSend: (xhr) ->
      $('canvas#graph_canvas').hide()
      showLoadingImg('graph_canvas_container', 'ldimg_graph_canvas')
    success: (data) ->

      $('#graph_dialog').fontSpy({
        callback: $.noop
      } )

      $('#ldimg_graph_canvas').remove()
      $('canvas#graph_canvas').show()
      drawChartInit('graph_canvas', data['config'], data['data'], data['memos'])

      # デフォルトでキャンバスを画面にフィットさせるための設定。
      # 2015-01-12 グラフ編集時には自動縮小は行わないため以下を削除。
      #$('#graph_canvas').css('width','100%')
      #$('#graph_canvas').css('max-width', data['config'].width+'px')
      #$('#graph_canvas').css('height','auto')
    error: (data) ->
      $('#ldimg_graph_canvas').remove()
      showMsgTip('An Error has occurred.', 'error')
  })
