$ ->

  graph_id = $("#graph_id").val()
  $.ajax({
    type: "GET",
    url: "/graphs/" + graph_id + "/load",
    beforeSend: (xhr) ->
      $('canvas#graph_canvas').hide()
      showLoadingImg('graph_canvas_container', 'ldimg_graph_canvas')
    success: (data) ->

      $('#graph_dialog').fontSpy({
        callback: $.noop
      } )

      $('#ldimg_graph_canvas').remove()
      $('canvas#graph_canvas').show()
      drawChartInit('graph_canvas', data['config'], data['data'], data['memos'])

      $('#graph_canvas').css('width','100%')
      $('#graph_canvas').css('max-width', data['config'].width+'px')
      $('#graph_canvas').css('height','auto')
    error: (data) ->
      $('#ldimg_graph_canvas').remove()
      showMsgTip('An Error has occurred.', 'error')
  })
