class TopController < ApplicationController

  def index

    unless current_user.blank?
      redirect_to :controller => :home, :action => :show
    end
    
  end

end
