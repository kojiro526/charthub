class MembersController < ApplicationController

  def show

    login_name = params[:login]
    @user = User.find(:first, :conditions => ["login = ?", login_name])

    @series = Series.includes(:user).where("users.login = ?", login_name).order("series.created_at DESC").limit(5)
    @series_collections = SeriesCollection.includes(:user).where("users.login = ?", login_name).order("series_collections.created_at DESC").limit(5)
    @graphs = Graph.includes(:user).where("users.login = ?", login_name).order("graphs.created_at DESC").limit(5)

  end

  def series

    login_name = params[:login]

    # TODO: 自分自身なら非公開を含めて取得できるようにするべき。
    @user = User.where(["login = ?", login_name]).first

    if request.xhr?
      if params[:keyword].blank?
        @series = Series.includes(:user).where("series.user_id = ?", @user.id)
      else
        @series = Series.includes(:user).where("series.user_id = ?", @user.id).where("series.name like '%#{params[:keyword]}%'").order("series.name")
      end
    else
      @series = Kaminari.paginate_array(Series.includes(:user).where("users.login = ?", login_name).order("series.updated_at DESC"))
      @series = @series.page(params[:page]).per(50)
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @series }
    end

  end

  def series_collections

    login_name = params[:login]
    @user = User.where(["login = ?", login_name]).first
    @series_collections = Kaminari.paginate_array(SeriesCollection.includes(:user).where("users.login = ?", login_name).order("series_collections.updated_at DESC"))
    @series_collections = @series_collections.page(params[:page]).per(50)

  end

  def graphs

    login_name = params[:login]
    @user = User.where(["login = ?", login_name]).first
    @graphs = Kaminari.paginate_array(Graph.includes(:user).where("graphs.user_id = ?", @user.id).order("graphs.updated_at DESC"))
    @graphs = @graphs.page(params[:page]).per(20)

  end

end
