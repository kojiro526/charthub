class ApplicationController < ActionController::Base
  protect_from_forgery

  layout 'application'

  before_filter :get_locale

  rescue_from ActiveRecord::RecordNotFound, with: :url_not_found

private
  def url_not_found
    respond_to do |format|
      format.html { render template: 'errors/error_404', status: :not_found }
      format.json { render nothing: true, status: :not_found }
    end
  end

  def get_locale
    if request.headers['Accept-Language'].blank?
      I18n.locale = "ja"
    else
      I18n.locale = request.headers['Accept-Language'].scan(/^[a-z]{2}/).first
      if I18n.locale.blank?
        I18n.locale = "ja"
      end
    end
  end

  def get_by_id(p)
    return Object.const_get(p[:controller].singularize.camelize).find(p[:id])
  end

  # パラメータに含まれるIDでコントローラに対応するモデルを検索し、存在をチェックするフィルタ。
  def check_exist
    obj = get_by_id(params)

    # underscoreはコントローラ名から取得した変数名等に利用する文字列。
    # series, series_collection
    underscored = params[:controller].singularize
    self.instance_variable_set("@#{underscored}", obj)
  end

  # 編集、削除などは自分が作成したオブジェクトのみ操作できるよう権限チェックするフィルタ。
  def check_permission
    obj = get_by_id(params)

    # オブジェクトに対する権限をチェック
    if obj.user.id == current_user.id
      # underscoreはコントローラ名から取得した変数名等に利用する文字列。
      # series, series_collection
      underscored = params[:controller].singularize
      self.instance_variable_set("@#{underscored}", obj)
    else
      respond_to do |format|
        format.html { render template: 'errors/error_403', status: :forbidden }
        format.json { render nothing: true, status: :forbidden }
      end
    end

  end

end
