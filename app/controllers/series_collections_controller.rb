# -*- coding: utf-8 -*-
class SeriesCollectionsController < ApplicationController

  before_filter :authenticate_user!, :except => [:index, :show, :load]
  before_filter :check_permission, except: [:index, :show, :new, :create, :load, :get_desc, :get_series, :get_selected_rows]

  # GET /series_collections
  # GET /series_collections.json
  def index
    #@series_collections = SeriesCollection.all(:include => [:user])
    @series_collections = Kaminari.paginate_array(SeriesCollection.includes(:user).order('updated_at DESC'))
    @series_collections = @series_collections.page(params[:page]).per(50)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @series_collections }
    end
  end

  # GET /series_collections/1
  # GET /series_collections/1.json
  def show
    @series_collection = SeriesCollection.find(params[:id])
    @series_table = @series_collection.series_table
    @sources = @series_collection.sources

   # TODO: 2014-10-15 使われておらず不要と思われるので削除。
   # @graph_config = CHART_CONFIG #グラフパラメータの初期値を設定。
   # @graph_config[:title] = @series_collection.name

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @series_collection }
    end
  end

  # GET /series_collections/new
  # GET /series_collections/new.json
  def new
    @series_collection = SeriesCollection.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @series_collection }
    end
  end

  # GET /series_collections/1/edit
  def edit
    #@series_collection = SeriesCollection.find(params[:id])
  end

  # POST /series_collections
  # POST /series_collections.json
  def create
    @series_collection = SeriesCollection.new(params[:series_collection])
    @series_collection.user_id = current_user.id

    respond_to do |format|
      if @series_collection.save
        format.html { redirect_to @series_collection, notice: 'Table was successfully created.' }
        format.json { render json: @series_collection, status: :created, location: @series_collection }
      else
        format.html { render action: "new" }
        format.json { render json: @series_collection.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /series_collections/1
  # PUT /series_collections/1.json
  def update
    #@series_collection = SeriesCollection.find(params[:id])

    respond_to do |format|
      if @series_collection.update_attributes(params[:series_collection])
        format.html { redirect_to @series_collection, notice: 'Table was successfully updated.' }
        #format.json { head :no_content }
        format.json { render json: @series_collection, status: :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @series_collection.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /series_collections/1
  # DELETE /series_collections/1.json
  def destroy
    #@series_collection = SeriesCollection.find(params[:id])
    login = @series_collection.user.login
    @series_collection.series_orders.clear
    @series_collection.series_calculators.clear
    @series_collection.graphs.clear
    @series_collection.destroy

    respond_to do |format|
      #format.html { redirect_to series_collections_url }
      format.html { redirect_to login_home_path }
      format.json { head :no_content }
    end
  end

  def load

    @series_collection = SeriesCollection.find(params[:id])
    series_table = @series_collection.series_table

    # デフォルト値をセット
    @graph_config = Hash.new
    @graph_config = MyLib::MyCcchartConfig.default_to_chart
    @graph_config[:title] = @series_collection.name

    #render json: {data: @series_collections.to_json_for_chart, config: @graph_config, status: 200}
    render json: {data: series_table.to_json_for_chart, config: @graph_config, status: 200}

  end

  def get_series

    series_collection = SeriesCollection.find(params[:id])

    respond_to do |format|
      format.json { render json: series_collection.series_orders.to_json(:include => [:series]) }
    end

  end

  # 系列の選択ボックスで、コレクションとして選択されている
  # 系列を全て取得する。
  # 実体のある系列と計算系列を別々に取得してsequenceでソート
  # してから返却する。
  def get_selected_rows

    series_collection = SeriesCollection.find(params[:id])

    arr = Array.new

    # 系列を取得してハッシュに追加。
    series_collection.series_orders.each{|order|
      arr.push({
        row_type: 'series',
        sequence: order.sequence,
        series_order_id: order.id, 
        series_id: order.series_id, 
        name: order.series_with_deleted.name,
        alias: order.alias,
        category_axis_type: order.series_with_deleted.category_axis_type
      })
    }

    # 計算系列を取得してハッシュに追加。
    series_collection.series_calculators.each{|culc|
      arr.push({
        row_type: 'series_calculator',
        sequence: culc.sequence,
        series_calculator_id: culc.id, 
        name: culc.name,
        expression: culc.expression
      })
    }

    arr.sort!{|a, b|
      a[:sequence] <=> b[:sequence]
    }

    respond_to do |format|
      format.json { render json: arr.to_json }
    end

  end

  # 選択された系列から、
  # 実体のある系列（series）と実体を持たない計算系列（series_calculator)
  # の2種類の情報を受け取り、それぞれをDBに登録する。
  def update_collection

    #series_collection = SeriesCollection.find(params[:id])
    series_collection = @series_collection

    SeriesCollection.transaction do

    # 既存の情報を削除
    series_collection.series_orders.destroy_all
    #series_collection.series_calculators.destroy_all

    # 受け取った系列と計算系列の順序を記録する。
    # これによって、系列と計算系列を別々にDBからselectした後で並び替える事ができる。
    params_series_order = Array.new
    #params_series_calculator = Array.new
    create_tmp = Array.new
    update_tmp = Array.new
    params[:collection].each_with_index{|col, i|
      case col[:row_type]
      when 'series'
        params_series_order.push({
          series_id: col[:series_id],
          sequence: i,
          alias: col[:alias]
        })
      when 'series_calculator'
        mode_tmp = col[:series_calculator_id].blank? ? 'new' : 'update'
        case mode_tmp
        when 'new'
          create_tmp.push({
            name: col[:name],
            sequence: i,
            expression: col[:expression]
          })
        when 'update'
          update_tmp.push({
            id: col[:series_calculator_id].to_i,
            name: col[:name],
            sequence: i,
            expression: col[:expression]
          })
        end
      end
    }

    check_selected_series(params_series_order)

    # 実体系列
    if params_series_order.size > 0
      series_orders = series_collection.series_orders.build(params_series_order)
      series_orders.each{|series_order|
        series_order.save!
      }
    end

    # 計算系列
      # 削除
      destroy_ids = []
      series_collection.series_calculator_ids.each{|id|
        fg = false
        update_tmp.each{|culc|
          if id == culc[:id]
            fg = true
          end
        }
        destroy_ids << id unless fg
      }
      SeriesCalculator.destroy(destroy_ids) if destroy_ids.size > 0

      # 新規,更新
      #新規
      if create_tmp.size > 0
        series_calculators = series_collection.series_calculators.build(create_tmp)
        series_calculators.each{|culc| culc.save! }
      end 

      #更新
      update_tmp.each{|culc|
        # UPD: 2014-10-16 元のテーブルに紐付いた計算系列であるかをここでチェック
        #      悪意のあるリクエストにより異なるテーブルに紐付いた計算系列をアップデートされないため。
        series_calculator = SeriesCalculator.find(culc[:id])
        if series_calculator.series_collection_id == series_collection.id
          series_calculator.attributes = culc
          series_calculator.save!
        end
      }

      series_collection.updated_at = Time.now
      series_collection.save!

    end #transaction

    respond_to do |format|
      format.json { render json: {notice: 'Table was successfully updated.'}}
    end

  rescue MyLib::MyInvalidSeriesSelectedException => e
    respond_to do |format|
      format.json { render json: {notice: e.message}}
    end
  end

  def get_desc

    series = SeriesCollection.find(params[:id])
    renderd = view_context.safe_text(series.description)

    render text: renderd

  end

private
  def check_selected_series(params_orders)
    series = Array.new
    params_orders.each{|order|
      begin
        series.push( Series.find(order[:series_id]) )
      rescue ActiveRecord::RecordNotFound => e
        raise MyLib::MyInvalidSeriesSelectedException.exception("存在しない系列が含まれています。")
      end
    }

    series_tmp = nil
    series.each_with_index{|s, i|
      if i == 0 
        series_tmp = s
      else
        if series_tmp.category_axis_type != s.category_axis_type
          raise MyLib::MyInvalidSeriesSelectedException.exception("異なる種別の項目ラベルが混在しています。")
        end
        series_tmp = s
      end 
    }

    series_ids = Array.new
    series.each{|s|
      if series_ids.include?(s.id)
        raise MyLib::MyInvalidSeriesSelectedException.exception("同じ系列が重複しています。")
      else
        series_ids.push(s.id)
      end
    }
  end
end
