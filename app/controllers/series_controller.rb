class SeriesController < ApplicationController

  before_filter :authenticate_user!, except: [:index, :show, :load, :get_desc]
  before_filter :check_exist, only: [:show, :load, :get_desc]
  #before_filter :check_permission, except: [:index, :show, :new, :create, :load, :get_desc]
  before_filter :check_permission, only: [:edit, :update, :destroy, :create_values]

  # GET /series
  # GET /series.json
  def index
    @series = Kaminari.paginate_array(Series.includes(:user).order('updated_at DESC'))
    @series = @series.page(params[:page]).per(50)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @series }
    end
  end

  # GET /series/1
  # GET /series/1.json
  def show
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @series }
    end
  end

  # GET /series/new
  # GET /series/new.json
  def new
    @series = Series.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @series }
    end
  end

  # GET /series/1/edit
  def edit
  end

  # POST /series
  # POST /series.json
  def create
    @series = Series.new(params[:series])
    @series.user_id = current_user.id
    #@series.series_type = 'series' # series_type自体不要となったはずなので後々DBから削除予定。2014-12-06

    respond_to do |format|
      if @series.save
        #format.html { redirect_to @series, notice: 'Series was successfully created.' }
        format.html { redirect_to login_series_show_path(login: @series.user.login, id: @series.id), notice: 'Series was successfully created.' }
        format.json { render json: @series, status: :created, location: @series }
      else
        format.html { render action: "new" }
        format.json { render json: @series.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /series/1
  # PUT /series/1.json
  def update

    #万が一、パラメータに故意に挿入された場合を想定して削除。
    params[:series].delete('category_axis_type')

    respond_to do |format|
      if @series.update_attributes(params[:series])
        format.html { redirect_to login_series_show_path(login: @series.user.login, id: @series.id), notice: 'Series was successfully updated.' }
        #format.json { head :no_content }
        format.json { render json: @series, status: :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @series.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /series/1
  # DELETE /series/1.json
  # TODO: valuesも含めて削除されるようにすること。 DONE
  # TODO: テーブルに含まれる等の情報を事前に出す？
  def destroy
    login = @series.user.login
    if @series.is_filter
    else 
      @series.values.clear
    end
    @series.graphs.clear
    @series.destroy # series_filtersは:dependentで削除される。

    respond_to do |format|
      # TODO: リダイレクト先を変える（HOMEの系列等？） DONE
      #format.html { redirect_to series_index_url }
      format.html { redirect_to login_home_path }
      format.json { head :no_content }
    end
  end

  # 値を更新
  def create_values

    #@series = Series.find(params[:id].to_i, :include => [:values])

    ActiveRecord::Base.transaction do

      @series.values.destroy_all
      @series.create_values_by_text(params[:values_by_text], params[:delimitter])
      @series.updated_at = Time.now
      @series.save!

    end #transaction

    respond_to do |format|
      format.html { redirect_to login_series_show_path(:login => current_user.login, :id => @series.id), notice: 'Value was successfully created.' }
#      format.json { render json: @value, status: :created, location: @value }
    end

  rescue ActiveRecord::RecordInvalid => e

    @series = Series.find(params[:id].to_i, :include => [:values])

    respond_to do |format|
      format.html { render action: "show" }
      format.json { render json: @value.errors, status: :unprocessable_entity }
    end

  end

  def load

    #@series = Series.find(params[:id])

    # デフォルト値をセット
    @graph_config = MyLib::MyCcchartConfig.default_to_chart
    @graph_config[:title] = @series.name

    render json: {data: @series.to_json_for_chart, config: @graph_config, status: 200}
=begin
    respond_to do |format|
      format.html { redirect_to series_index_url }
      format.json { render json: @series.to_json_for_chart }
    end
=end

  end

  # 詳細画面でdescriptionをAjaxで取得する。
  def get_desc

    renderd = view_context.safe_text(@series.description)

    render text: renderd

  end

  # フォームから受け取ったテキストをパースして値を検証する。
  def preview_values

    data = Hash.new
    if params[:series_id].blank?
      series = Series.new 
    else
      series = Series.find(params[:series_id])
    end

    begin
      values = series.build_values_by_text(params[:values_by_text], params[:delimitter])    
      html = render_to_string partial: 'series/table', object: series, locals: {values: values}
      data[:html] = html
      render json: data
    rescue MyException::MyParsingError => ex
      html = ex.message.split("\n").map{|line|
        ERB::Util.h(line)
      }.join('</br>')
      render text: html, status: 400 
    rescue => ex
      logger.error("#{ex.message}\n#{ex.backtrace}")
      html = "ERROR"
      render text: html, status: 400 
    end

  end

private

end
