class GraphsController < ApplicationController

  before_filter :authenticate_user!, :except => [:index, :show, :load, :image]
  before_filter :check_permission, except: [:index, :show, :new, :new_for_series, :new_for_series_collection, :create, :load, :image, :new_memo, :config_test_new, :config_test_edit]

  # GET /graphs
  # GET /graphs.json
  def index
    @graphs = Graph.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @graphs }
    end
  end

  # GET /graphs/1
  # GET /graphs/1.json
  def show
    @graph = Graph.find(params[:id])
    #@series_table = @graph.graphable.series_table

    if @graph.graphable_type == "Series"
      @series_table = @graph.graphable.series_table
      @series_type = "series"
    elsif @graph.graphable_type == "SeriesCollection"
      # テーブルの場合はグラフ用に選択された表示オプションをセット。
      @series_table = @graph.graphable.series_table_with_view_options(@graph.get_all_series_options)
      @series_type = "series_collections"
    end

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @graph }
    end
  end

  # GET /graphs/new
  # GET /graphs/new.json
=begin
  def new
    @graph = Graph.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @graph }
    end
  end
=end

  def new_for_series
    @series = Series.find(params[:series_id])
    @series_type = "series"
    @action_type = "new"

    # デフォルトの系列オプションンを取得
    @selected_series = @series.get_all_series_options

    # グラフの新規作成時は全ての系列が表示される。
    @selected_series.each{|series|
      series.selected=true
    }

    # 初期化
    graph_default = MyLib::MyCcchartConfig.default
    @graph = @series.graphs.build([graph_default])[0]

    # 範囲選択のリスト用に項目軸ラベルの一覧を取得。
    @category_axis_labels = Hash.new
    @series_table = @graph.graphable.series_table
    @series_table.category_axis.labels.each{|label|
      @category_axis_labels[label.text] = label.text
    }

    # 新規作成時デフォルトは空
    @choice_list =  MyLib::MyChoiceList.new

    @graph.chart_type = 'line'
    @graph.title = @series.name

    respond_to do |format|
      format.html { render :action => 'new' }
      format.json { render json: @graph }
    end
  end

  def new_for_series_collection
    @series = SeriesCollection.find(params[:series_collection_id])
    @series_type = "series_collections"
    @action_type = "new"

    # デフォルトの系列オプションを取得
    @selected_series = @series.get_all_series_options

    # グラフの新規作成時は全ての系列が表示される。
    @selected_series.each{|series|
      series.selected=true
    }

    # 初期化
    graph_default = MyLib::MyCcchartConfig.default
    @graph = @series.graphs.build([graph_default])[0]

    # 範囲選択のリスト用に項目軸ラベルの一覧を取得。
    @category_axis_labels = Hash.new
    @series_table = @graph.graphable.series_table_with_view_options(@selected_series)
    @series_table.category_axis.labels.each{|label|
      @category_axis_labels[label.text] = label.text
    }

    # 新規作成時デフォルトは空
    @choice_list =  MyLib::MyChoiceList.new

    @graph.chart_type = 'line'
    @graph.title = @series.name

    respond_to do |format|
      format.html { render :action => 'new' }
      format.json { render json: @graph }
    end
  end

  # GET /graphs/1/edit
  def edit
    #@graph = Graph.find(params[:id])

    @series = @graph.graphable
    if @graph.graphable_type == "Series"
      @series_type = "series"
    elsif @graph.graphable_type == "SeriesCollection"
      @series_type = "series_collections"
    end
    @graph_id = @graph.id
    @action_type = "edit"

    @selected_series = @graph.get_all_series_options

    # 項目軸ラベルの一覧を取得。
    @category_axis_labels = Hash.new
    case @graph.graphable_type
    when "Series"
      @series_table = @graph.graphable.series_table
    when "SeriesCollection"
      @series_table = @graph.graphable.series_table_with_view_options(@selected_series)
    end
    @series_table.category_axis.labels.each{|label|
      @category_axis_labels[label.text] = label.text
    }

    # 行の個別指定設定を取得
    @choice_list =  @graph.get_choice_list

    # TODO: この行は不要？
    # 初期化
    graph_default = Hash.new
    CHART_CONFIG.each{|key, val| graph_default[key.to_s.underscore] = val unless key == :type }

  end

  # POST /graphs
  # POST /graphs.json
  def create
    #@graph = Graph.new(params[:graph])
    case params[:series_type]
    when 'series'
      @series = Series.find(params[:graph][:graphable_id])
    when 'series_collections'
      @series = SeriesCollection.find(params[:graph][:graphable_id])
    end

    ActiveRecord::Base.transaction do

      # 初期化
      #graph_default = Hash.new
      #CHART_CONFIG.each{|key, val| graph_default[key.to_s.underscore] = val unless key == :type }
      graph_default = MyLib::MyCcchartConfig.default
      @graph = @series.graphs.build([graph_default])[0]
      #@graph.attributes = params[:graph]
      @graph.attributes_from_params = params[:graph]
      @graph.user_id = current_user.id
      @graph.set_series_option = params[:series_op]

      @graph.graph_image = Base64.decode64(params["graph_image"].gsub(/data:image\/png\;base64\,/, ''))
      @graph.save!
      params[:ccchart_memo].each{|memo|
        ccchart_memo = @graph.ccchart_memos.build
        ccchart_memo.attributes_from_params = memo
        ccchart_memo.save!
      }

    end

    respond_to do |format|
      case params[:series_type]
      when 'series'
        format.html { redirect_to login_series_show_path(login: @series.user.login, id: @series.id), notice: 'Graph was successfully created.' }
      when 'series_collections'
        format.html { redirect_to login_series_collection_show_path(login: @series.user.login, id: @series.id), notice: 'Graph was successfully created.' }
      end
      format.json { render json: @graph, status: :created, location: @graph }
    end

  rescue => ex

    logger.error(ex.message)
    logger.error(ex.backtrace.join("\n"))

    respond_to do |format|
      format.html { render action: "new" }
      format.json { render json: @graph.errors, status: :unprocessable_entity }
    end
  end

  # PUT /graphs/1
  # PUT /graphs/1.json
  def update
    #@graph = Graph.find(params[:id])

    ActiveRecord::Base.transaction do
      @graph.attributes_from_params = params[:graph]
      @graph.set_series_option = params[:series_op]
      @graph.graph_image = Base64.decode64(params["graph_image"].gsub(/data:image\/png\;base64\,/, ''))
      @graph.save!
      #@graph.ccchart_memos.clear
      CcchartMemo.where('graph_id=?', @graph.id).delete_all
      params[:ccchart_memo].each{|memo|
        ccchart_memo = @graph.ccchart_memos.build
        ccchart_memo.attributes_from_params = memo
        ccchart_memo.save!
      }
    end

    respond_to do |format|
      format.html { redirect_to @graph, notice: 'Graph was successfully updated.' }
      format.json { head :no_content }
    end
  rescue => ex

    logger.error(ex.message)
    logger.error(ex.backtrace.join("\n"))

    @series = @graph.graphable
    if @graph.graphable_type == "Series"
      @series_type = "series"
    elsif @graph.graphable_type == "SeriesCollection"
      @series_type = "series_collections"
    end
    @graph_id = @graph.id
    @action_type = "edit"

    @selected_series = @graph.get_all_series_options
    respond_to do |format|
      format.html { render action: "edit" }
      format.json { render json: @graph.errors, status: :unprocessable_entity }
    end

=begin
    respond_to do |format|
      #if @graph.update_attributes(params[:graph])
      if @graph.save
        format.html { redirect_to @graph, notice: 'Graph was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @graph.errors, status: :unprocessable_entity }
      end
    end
=end
  end

  # DELETE /graphs/1
  # DELETE /graphs/1.json
  def destroy
    #@graph = Graph.find(params[:id])
    login = @graph.user.login
    graphable_type = @graph.graphable_type
    graphable_id = @graph.graphable_id
    @graph.ccchart_memos.delete_all
    @graph.graph_series.delete_all
    @graph.destroy

    respond_to do |format|
      #format.html { redirect_to graphs_url }
      case graphable_type
        when 'Series'
          format.html { redirect_to login_series_show_path(login: login, id: graphable_id) }
        when 'SeriesCollection'
          format.html { redirect_to login_series_collection_show_path(login: login, id: graphable_id) }
        else
          format.html { redirect_to login_home_path }
      end
      format.json { head :no_content }
    end
  end

  def load

    @graph = Graph.find(params[:id])

    # デフォルト値をセット
    #@graph_config = Hash.new
    @graph_config = @graph.to_config
    all_series_options = @graph.get_all_series_options
    all_series_options.to_config.each{|key,val|
      if key == :useVal
        @graph_config[key] = val if @graph_config[:useVal] == "no"
      else
        @graph_config[key] = val
      end
    }
    case @graph.graphable_type
    when "Series"
      tmp_table = @graph.graphable.series_table

      # 範囲選択や任意選択による絞り込み
      case @graph.value_select_mode 
      when 1
        data_tmp = tmp_table.range(@graph.select_from, @graph.select_to).to_json_for_chart
      when 2
        data_tmp = tmp_table.choice(@graph.get_choice_list).to_json_for_chart
      else
        data_tmp = tmp_table.to_json_for_chart
      end
    when "SeriesCollection"

      op = @graph.get_series_option
      tmp_table = @graph.graphable.series_table_with_view_options(op)

      # 範囲選択や任意選択による絞り込み
      case @graph.value_select_mode 
      when 1
        data_tmp = tmp_table.range(@graph.select_from, @graph.select_to).to_json_for_chart_with_option(op)
      when 2
        data_tmp = tmp_table.choice(@graph.get_choice_list).to_json_for_chart_with_option(op)
      else
        data_tmp = tmp_table.to_json_for_chart_with_option(op)
      end
    end

    # メモの取得
    @ccchart_memos = Array.new
    @graph.ccchart_memos.each{|memo|
      @ccchart_memos += memo.to_config
    }

    #render json: {data: data_tmp, config: @graph_config, graph_id: @graph.id, status: 200}
    render json: {data: data_tmp, config: @graph_config, memos: @ccchart_memos, graph_id: @graph.id, status: 200}

=begin
    respond_to do |format|
      format.html { redirect_to series_index_url }
      format.json { render json: @series.to_json_for_chart }
    end
=end

  end

  # POST
  def config_test_new

    @graph = Graph.new
=begin
    @graph_config = get_test_config(@graph)
    @memos = get_test_memos

    series_options = MyLib::MySeriesOptions.create_by_params(params[:series_op])
    # 系列の選択状態が変更されたかされていないかを判定して処理する。
    if series_options.selected_is_changed?
      # 系列の選択状態が変化した場合、チャート用のデータを取得し直す。
      case @graph.graphable_type
      when "Series"
        data_tmp = @graph.graphable.to_json_for_chart
      when "SeriesCollection"
        data_tmp = @graph.graphable.to_json_for_chart_with_option(series_options)
      end

      # 範囲選択用の項目軸ラベルを取得し直す。
      category_axis = @graph.graphable.category_axis
      labels_tmp = Hash.new
      category.axis.labels.each{|label|
	# label.textはvalue.labelが元になっている。
	# TODO: 検索用の値と表示用のフォーマットされたテキストを分ける場合はtemporary_tableの仕様から修正する必要あり。
        labels_tmp[label.text] = label.text
      }
    else
      labels_tmp = nil
      data_tmp = nil
    end

    render json: {data: data_tmp, config: @graph_config, memos: @memos, labels: labels_tmp, graph_id: nil , status: 200}
=end
    json_data = get_json(@graph)
    json_data[:graph_id] = nil
    json_data[:status] = 200
    render json: json_data
  end

  def config_test_edit
    @graph = Graph.find(params[:id])
=begin
    @graph_config = get_test_config(@graph)
    @memos = get_test_memos

    series_options = MyLib::MySeriesOptions.create_by_params(params[:series_op])
    # 系列の選択状態が変更されたかされていないかを判定して処理する。
    if series_options.selected_is_changed?
      # 系列の選択状態が変化した場合、チャート用のデータを取得し直す。
      case @graph.graphable_type
      when "Series"
        data_tmp = @graph.graphable.to_json_for_chart
      when "SeriesCollection"
        data_tmp = @graph.graphable.to_json_for_chart_with_option(series_options)
      end

      # 範囲選択用の項目軸ラベルを取得し直す。
      labels_tmp = @graph.graphable.category_axis
    else
      labels_tmp = nil
      data_tmp = nil
    end

    render json: {data: data_tmp, config: @graph_config, memos: @memos, labels: labels_tmp, graph_id: @graph.id, status: 200}
=end
    json_data = get_json(@graph)
    json_data[:graph_id] = @graph.id
    json_data[:status] = 200
    render json: json_data
  end

  def get_json(graph)
    res = Hash.new

    res[:config] = get_test_config(graph)
    res[:memos]  = get_test_memos

    series_options = MyLib::MySeriesOptions.create_by_params(params[:series_op])
    # 系列の選択状態が変更されたかされていないかを判定して処理する。
    if series_options.selected_is_changed?
      # 系列の選択状態が変化した場合、チャート用のデータを取得し直す。
      case graph.graphable_type
      when "Series"
        tmp_table = graph.graphable.series_table

        # 範囲選択や任意選択による絞り込み
        case graph.value_select_mode 
        when 1
          data_tmp = tmp_table.range(graph.select_from, graph.select_to).to_json_for_chart
        when 2
          data_tmp = tmp_table.choice(graph.get_choice_list).to_json_for_chart
        else
          data_tmp = tmp_table.to_json_for_chart
        end
        #data_tmp = graph.graphable.to_json_for_chart
      when "SeriesCollection"

        op = graph.get_series_option
        tmp_table = graph.graphable.series_table_with_view_options(op)

        # 範囲選択や任意選択による絞り込み
        case graph.value_select_mode 
        when 1
          data_tmp = tmp_table.range(graph.select_from, graph.select_to).to_json_for_chart_with_option(op)
        when 2
          data_tmp = tmp_table.choice(graph.get_choice_list).to_json_for_chart_with_option(op)
        else
          data_tmp = tmp_table.to_json_for_chart_with_option(op)
        end
        #data_tmp = graph.graphable.to_json_for_chart_with_option(series_options)
      end

      # 範囲選択用の項目軸ラベルを取得し直す。
      category_axis = graph.graphable.series_table_with_view_options(series_options).category_axis
      labels_tmp = Hash.new
      category_axis.labels.each{|label|
	# label.textはvalue.labelが元になっている。
	# TODO: 検索用の値と表示用のフォーマットされたテキストを分ける場合はtemporary_tableの仕様から修正する必要あり。
        labels_tmp[label.text] = label.text
      }
    else
      labels_tmp = nil
      data_tmp = nil
    end

    res[:data] = data_tmp
    res[:labels] = labels_tmp
    return res
    #render json: {data: data_tmp, config: @graph_config, memos: @memos, labels: labels_tmp, graph_id: @graph.id, status: 200}
  end

  def image
    graph = Graph.find(params[:id])
    send_data graph.graph_image, type: 'image/png', disposition: :inline
  end

  def new_memo
    if params[:counter].blank?
      next_count = 0
    else
      next_count = params[:counter].to_i
    end

    html = render_to_string partial: 'graphs/form_memo', collection: [CcchartMemo.new(font: {size: 12}, line_space: 6)], locals: {next_count: next_count}
    render json: {html: html}
  end

private
  def get_test_config(graph)
    graph.attributes_from_params = params[:graph]
    graph.user_id = current_user.id
    #@graph.set_series_option = params[:series_op]

    #return @graph.to_config
    return @graph.to_config_with_series_option(params[:series_op])
  end

  def get_test_memos
    memo_tmp = Array.new
    params[:ccchart_memo].each{|memo|
      memo_lines = memo['val'].gsub(/\r\n/, "\n").gsub(/\r/, "\n").split("\n")
      memo_lines.each_with_index{|line, i|
        memo_tmp.push( {
          val: line,
          left: memo['left'].to_i,
          top: memo['top'].to_i + memo['font']['size'].to_i + (memo['font']['size'].to_i + memo['line_space'].to_i) * i,
          color: memo['color'],
          #font: "100 #{memo['font']['size']}px #{memo['font']['font']}"
          font: "200 #{memo['font']['size']}px 'Noto Sans Japanese'"
        } )
      }
    }
    return memo_tmp
  end
end
