# -*- coding: utf-8 -*-
class ValuesController < ApplicationController
  # GET /values
  # GET /values.json
=begin
  def index
    @values = Value.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @values }
    end
  end
=end

  # GET /values/1
  # GET /values/1.json
=begin
  def show
    @value = Value.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @value }
    end
  end
=end

  # GET /values/new
  # GET /values/new.json
=begin
  def new
    @value = Value.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @value }
    end
  end
=end

  # GET /values/1/edit
=begin
  def edit
    @value = Value.find(params[:id])
  end
=end

  # POST /values
  # POST /values.json
=begin
  def create
#    @value = Value.new(params[:value])

    @series = Series.find(params[:series_id].to_i, :include => [:values])

    ActiveRecord::Base.transaction do

      @series.values.destroy_all

      input_values = params[:values_by_text]
      i=1
      delimitter = {'comma' => ',', 'tab' => "\t"}[params[:delimitter]]
      input_values.split("\n").each{|line|
        label,value,note = line.split(delimitter)
        value_tmp = Value.new
        value_tmp.category_axis_type = @series.category_axis_type
        value_tmp.series_id = @series.id
        value_tmp.sequence = i
        value_tmp.value = value.gsub(/\r/, '').gsub(/\s/,'').gsub(/\,/,'')
        value_tmp.label = label
        value_tmp.note = note
        value_tmp.save!
        i = i+1
      }

      @series.updated_at = Time.now
      @series.save!

    end #transaction

    respond_to do |format|
      format.html { redirect_to login_series_show_path(:login => current_user.login, :id => @series.id), notice: 'Value was successfully created.' }
#      format.json { render json: @value, status: :created, location: @value }
    end

  rescue ActiveRecord::RecordInvalid => e

    respond_to do |format|
      format.html { render template: "series/show" }
      format.json { render json: @value.errors, status: :unprocessable_entity }
    end

  end
=end

  # PUT /values/1
  # PUT /values/1.json
=begin
  def update
    @value = Value.find(params[:id])

    respond_to do |format|
      if @value.update_attributes(params[:value])
        format.html { redirect_to @value, notice: 'Value was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @value.errors, status: :unprocessable_entity }
      end
    end
  end
=end

  # DELETE /values/1
  # DELETE /values/1.json
=begin
  def destroy
    @value = Value.find(params[:id])
    @value.destroy

    respond_to do |format|
      format.html { redirect_to values_url }
      format.json { head :no_content }
    end
  end
=end
end
