# -*- coding: utf-8 -*-
class SeriesFiltersController < ApplicationController

  before_filter :authenticate_user!, :expect => [:show]

  # GET /series_filters
  # GET /series_filters.json
  # MEMO: 不要なアクションの為削除。
=begin
  def index
    @series_filters = SeriesFilter.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @series_filters }
    end
  end
=end

  # GET /series_filters/1
  # GET /series_filters/1.json
  # MEMO: 不要なアクションの為削除。
=begin
  def show
    @series_filter = SeriesFilter.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @series_filter }
    end
  end
=end

  # GET /series_filters/new
  # GET /series_filters/new.json
  def new

    @series = Series.new
    @series.build_series_filter
    @target_series = Series.find_available_with_user_id(current_user.id)
    #@filter_type = [['相対化', 'relativezation'],['相対化（逆数）', 'relativezation_reciprocal']]
    @filter_type = MY_APP['filter_type'].map{|key,val| [val,key]}
    @reference_values = Hash.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @series_filter }
    end
  end

  # GET /series_filters/1/edit
  def edit

    @series = Series.find(params[:id])
    @target_series = Series.find_available_with_user_id(current_user.id)
    #@filter_type = [['相対化', 'relativezation'],['変化量', 'amount_of_change'],['変化率','rate_of_change']]
    @filter_type = MY_APP['filter_type'].map{|key,val| [val,key]}
    @reference_values = Hash.new
    # TODO: 2014-10-17 元の系列が削除されている場合の処理を行うこと。
    referenced_series = Series.find(@series.filter_target_id)
    referenced_series.values.map{|val|
      @reference_values["#{val.label}:#{val.value}"] = val.label
    }
#    @series_filter = SeriesFilter.find(params[:id])
  end

  # POST /series_filters
  # POST /series_filters.json
  def create
    @series = Series.new(params[:series])
    @series.user_id = current_user.id
    @series.is_filter = true

    respond_to do |format|
      if @series.save
        series_filter = @series.build_series_filter(params[:series_filter])
        if series_filter.save
          #format.html { redirect_to @series, notice: 'Series filter was successfully created.' }
          #format.html { redirect_to '/series_filters/new', notice: 'Series filter was successfully created.' }
          format.html { redirect_to login_series_show_path(login: @series.user.login, id: @series.id), notice: 'Series filter was successfully created.' }
          format.json { render json: @series, status: :created, location: @series }
        else
          format.html { render action: "new" }
          format.json { render json: series_filter.errors, status: :unprocessable_entity }
        end
      else
        format.html { render action: "new" }
        format.json { render json: @series.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /series_filters/1
  # PUT /series_filters/1.json
  def update
    #@series_filter = SeriesFilter.find(params[:id])
    @series = Series.find(params[:id])

    begin
      Series.transaction do
        @series.update_attributes!(params[:series])
        @series.series_filter.update_attributes!(params[:series_filter])
      end

      respond_to do |format|
        #format.html { redirect_to @series, notice: 'Series filter was successfully updated.' }
        format.html { redirect_to login_series_show_path(login: @series.user.login, id: @series.id), notice: 'Series filter was successfully updated.' }
        format.json { head :no_content }
      end

    rescue
      respond_to do |format|
        format.html { render action: "edit" }
        format.json { render json: @series.errors, status: :unprocessable_entity }
      end
    end

=begin
    respond_to do |format|
      if @series_filter.update_attributes(params[:series_filter])
        format.html { redirect_to @series_filter, notice: 'Series filter was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @series_filter.errors, status: :unprocessable_entity }
      end
    end
=end
  end

  # DELETE /series_filters/1
  # DELETE /series_filters/1.json
  # MEMO: series_filterはseriesの削除に連動して削除されるのでdestroyアクションは不要
=begin
  def destroy
    @series_filter = SeriesFilter.find(params[:id])
    @series_filter.destroy

    respond_to do |format|
      format.html { redirect_to series_filters_url }
      format.json { head :no_content }
    end
  end
=end
end
