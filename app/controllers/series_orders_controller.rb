class SeriesOrdersController < ApplicationController
  # GET /series_orders
  # GET /series_orders.json
  def index
    @series_orders = SeriesOrder.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @series_orders }
    end
  end

  # GET /series_orders/1
  # GET /series_orders/1.json
  def show
    @series_order = SeriesOrder.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @series_order }
    end
  end

  # GET /series_orders/new
  # GET /series_orders/new.json
  def new
    @series_order = SeriesOrder.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @series_order }
    end
  end

  # GET /series_orders/1/edit
  def edit
    @series_order = SeriesOrder.find(params[:id])
  end

  # POST /series_orders
  # POST /series_orders.json
  def create
    @series_order = SeriesOrder.new(params[:series_order])

    respond_to do |format|
      if @series_order.save
        format.html { redirect_to @series_order, notice: 'Series order was successfully created.' }
        format.json { render json: @series_order, status: :created, location: @series_order }
      else
        format.html { render action: "new" }
        format.json { render json: @series_order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /series_orders/1
  # PUT /series_orders/1.json
  def update
    @series_order = SeriesOrder.find(params[:id])

    respond_to do |format|
      if @series_order.update_attributes(params[:series_order])
        format.html { redirect_to @series_order, notice: 'Series order was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @series_order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /series_orders/1
  # DELETE /series_orders/1.json
  def destroy
    @series_order = SeriesOrder.find(params[:id])
    @series_order.destroy

    respond_to do |format|
      format.html { redirect_to series_orders_url }
      format.json { head :no_content }
    end
  end
end
