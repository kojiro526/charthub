class HomeController < ApplicationController
  before_filter :authenticate_user!

  def show

    @user = User.find(current_user.id)

    @series = Series.includes(:user).where("user_id = ?", current_user.id).order("series.updated_at DESC").limit(5)
    @series_collections = SeriesCollection.includes(:user).where("user_id = ?", current_user.id).order("series_collections.updated_at DESC").limit(5)
    @graphs = Graph.includes(:user).where("user_id = ?", current_user.id).order("graphs.updated_at DESC").limit(5)

  end


end
