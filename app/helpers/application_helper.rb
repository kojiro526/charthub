module ApplicationHelper

  # ユーザーがログインしておりかつそのユーザーが所有者であるかを判定。
  # user_idは判定の対象となるユーザーID
  def signed_in_and_yours?(user_id)
    if current_user.blank?
      return false
    else
      current_user.id === user_id
    end
  end

  def int_or_float?(val)

    if val =~ /^\d+.\d+$/
      return :float
    elsif val =~ /^\d+$/
      return :int
    else
      return :error
    end

  end

  # 説明文などを安全な文字列に整形するヘルパー
  def safe_text(txt)
    ERB::Util.html_escape(txt).gsub(/\r\n|\r|\n/, '<br />').html_safe
  end


end

# deviseのメールのローカライズ用
module Devise
  module Mailers
    module Helpers
    protected
      def template_paths
        template_path = _prefixes.dup
        template_path.unshift "#{@devise_mapping.scoped_path}/mailer" if self.class.scoped_views?
        template_path.unshift "#{@devise_mapping.scoped_path}/mailer/#{I18n.default_locale}" if self.class.scoped_views?
        template_path.unshift "devise/mailer/#{I18n.locale}"
        template_path
      end
    end
  end
end

