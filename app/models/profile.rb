class Profile < ActiveRecord::Base
  attr_accessible :introduction, :name, :user_id, :twitter_account, :include_twitter_account

  belongs_to :user

  def name_view

    if self.name.blank?
      if self.user.login.blank?
        return 'Edit Your Profile'
      else
        return self.user.login
      end
    else
      return self.name
    end
  end

end
