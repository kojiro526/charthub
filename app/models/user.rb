class User < ActiveRecord::Base

  has_many :series
  has_many :series_collections
  has_one  :profile
  has_many :graphs

  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  # ADD 2014-04-29 :authentication_keys => [:login] for devise
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
#         :recoverable, :rememberable, :trackable, :validatable,
#         :authentication_keys => [:login]

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me
  attr_accessible :login # ADD 2014-04-29 for devise
  # attr_accessible :title, :body
  
  after_create :create_user_profile

private
  def create_user_profile
    self.create_profile
  end

end
