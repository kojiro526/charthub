class Graph < ActiveRecord::Base
#  attr_accessible :series_id, :sub_title, :title, :user_id

  attr_accessible :user_id, :graphable_type, :graphable_id, :chart_type, 
    :value_select_mode, :select_from, :select_to, :select_choice,
    :change_r_c,
    :width, :height, :axis_x_width, :axis_x_len, :axis_y_width, :axis_y_len, 
    :only_chart, :only_chart_width_title, :padding_top, :padding_bottom, :padding_left, :padding_right, 
    :x_color, :y_color, :y_scale_skip, :x_scale_rotate, :y_scale_rotate, 
    :title, :title_color, :title_font, :title_text_align, :title_y, 
    :sub_title, :sub_title_color, :sub_title_font, :sub_title_text_align, :sub_title_y, 
    :x_scale_color, :x_scale_font, :x_scale_align, :x_scale_x_offset, :x_scale_y_offset, :x_scale_skip, 
    :col_names_title_offset, 
    :y_scale_color, :y_scale_font, :y_scale_align, :y_scale_x_offset, :y_scale_y_offset, :y_scale_percent, 
    :use_hanrei,
    :hanrei_color, :hanrei_font, :hanrei_align, :hanrei_line_height, :hanrei_x_offset, :hanrei_y_offset, 
    :hanrei_radius, :hanrei_marker_style, 
    :unit, :unit_color, :unit_font, :unit_align, :unit_x_offset, :unit_y_offset, :use_val, 
    :percent_val, :use_tool_tip, :bar_tip_anchor_color, :use_marker, :use_first_to_col_name, :use_first_to_row_name, 
    :max_y, :min_y, :rounded_up_max_y, :max_x, :min_x, :rounded_up_max_x, :round_digit,
    :color_set, :line_width, :border_width, :marker_width,
    :bg, :bg_gradient, 
    :bar_padding, :bar_width,
    :pie_data_index, :col_name_font, :pie_ring_width, :pie_hole_radius,
    :text_color,
    :use_shadow, :shadows,
    :image, :thumbnail
    #:series_option

  after_initialize :set_font_properties

  serialize :select_choice, Array
  serialize :bg_gradient, Hash
  serialize :title_font, Hash
  serialize :sub_title_font, Hash
  serialize :x_scale_font, Hash
  serialize :y_scale_font, Hash
  serialize :hanrei_font, Hash
  serialize :unit_font, Hash
  serialize :col_name_font, Hash
  serialize :color_set, Array
  serialize :shadows, Hash

  belongs_to :graphable, :polymorphic => true
  belongs_to :user, counter_cache: :graphs_count
  has_many :ccchart_memos

  has_many :graph_series

  def attributes_from_params=(params)
    pm = Hash.new
    params.each{|key, val|
      hash_tmp = Hash.new
      case key
      when "padding_top","padding_bottom","padding_left","padding_right"
        # 値がnilの場合は設定そのものをセットしない。
        unless val == nil || val == ""
          hash_tmp[key] = val
        end
      when "chart_area_select"
        case val
        when "all"
          hash_tmp = {only_chart: "no", only_chart_width_title: "no"}
        when "only_chart"
          hash_tmp = {only_chart: "yes", only_chart_width_title: "no"}
        when "chart_and_title"
          hash_tmp = {only_chart: "no", only_chart_width_title: "yes"}
        end
      when "use_val","percent_val","use_tool_tip","y_scale_percent","use_hanrei","change_r_c","use_shadow"
        # チェックボックス系のフォームはyes,noにコンバートする。
        #hash_tmp[key] = convBoolean(val)
        hash_tmp[key] = val
      when "bg_gradient"
        # paramsをHashに変換
        hash_tmp[key] = params_to_hash(val)
      when "title_font", "sub_title_font", "x_scale_font", "y_scale_font", "hanrei_font", "unit_font"
        # font系の設定を保存用に変換
        hash_tmp[key] = params_to_hash(val)
      when "color_set"
        # とりあえずパラメータの値をそのまま入力する。
        # TODO color_setのフォームを実装した際には修正すること。
        hash_tmp[key] = val
      else
        hash_tmp[key] = val
      end

      hash_tmp.each{|k, v|
        pm[k] = v
      }
    }
    self.attributes = pm
  end

  def to_config

    graph_config = Hash.new

    # 定数で設定された設定名以外は無視される。
    CHART_CONFIG.each{|key, val|
      key_tmp = key.to_s.underscore.to_sym
      # 設定名を上手く変換できなかったため特例
      # changeRC -> change_rc（本来はchange_r_cに変換されることを期待される）
      if key_tmp == :change_rc
        key_tmp = :change_r_c
      end

      if self.methods.include?(key_tmp)
        case key_tmp
        when :bg_gradient
          # Hash系の処理
          graph_config[key] = val
          self[key_tmp].each{|k, v|
            graph_config[key][k.to_sym] = v
          }
        when :color_set
          # Array系の処理
          #graph_config[key] = self.color_set
          # TODO: まずはデフォルト値を設定するようにした。後で直す。
          color_set_tmp = Array.new
          2.times{|i|
            color_set_tmp += val
          }
          graph_config[key] = color_set_tmp
        when :title_font, :sub_title_font, :x_scale_font, :y_scale_font, :hanrei_font, :unit_font, :col_name_font
          # font系の処理
          #graph_config[key] = "100 #{val[:size]}px #{val[:font]}"
          #if !self[key_tmp].blank? && self[key_tmp].class.name == "Hash"
          #  graph_config[key] = "100 #{self[key_tmp][:size]}px #{self[key_tmp][:font]}"
          #end
          graph_config[key] = "200 #{val[:size]}px 'Noto Sans Japanese'"
          if !self[key_tmp].blank? && self[key_tmp].class.name == "Hash"
            graph_config[key] = "200 #{self[key_tmp][:size]}px 'Noto Sans Japanese'"
          end
        else
          unless self[key_tmp].nil?
            # 値がnilの場合はハッシュに追加しない。（自動計算）
            case key_tmp
            when :use_val, :percent_val, :use_tool_tip, :y_scale_percent, :use_hanrei, :change_r_c, :use_shadow
              # Boolean型は0,1でDBに保存されているので、yes,noに変換
              graph_config[key] = convBoolean(self[key_tmp])
            else
              graph_config[key] = self[key_tmp]
            end
          end
        end
      else
        if key_tmp == :type
          graph_config[:type] = self[:chart_type]
        else
          graph_config[key] = val
        end
      end
    }

    return graph_config

  end

  # to_configでコンフィグを取得し、さらに系列ごとの設定を追加する。
  # colorSet
  # useVal,valFont,valColor,valYOffset,valXOffset
  def to_config_with_series_option(series_op)
    graph_config = self.to_config

    series_options = MyLib::MySeriesOptions.create_by_params(series_op)
    series_options.to_config.each{|key,val|
      # useValについては全体設定の方で有効になっていれば、系列個別の設定については無視する。
      if key == :useVal
        graph_config[key] = val if graph_config[:useVal] == "no"
      else
        graph_config[key] = val
      end
    }

    return graph_config
  end

  # JSON形式で保持されている系列の表示オプションをパースして取得
  # 保存された設定は、その後で系列が追加・削除されている可能性があるため、実際の系列と同じではない。
  # そのため、本メソッドで設定を取得する際に、まずグラフに紐付く系列の設定を取得し、それに
  # 保存されている設定を上書きすることで、グラフに紐付く全ての設定を取得するようにする。
  # UPD: 2014-11-14 グラフの系列の設定をJSONではなくテーブルから取得するよう修正。
  def get_series_option
    series_options = MyLib::MySeriesOptions.new
    self.graph_series.each{|s|
      series_options << s.to_series_option
    }
    return series_options
  end

  # グラフへの表示の有無を設定するオプションをJSON形式で
  # 保持
  # UPD 2014-11-13 JSONではなくテーブルに保存するよう修正。
  def set_series_option=(params)

    self.graph_series.each{|gs|
      gs.destroy
    }
    params.each_with_index{|p, i|
      if p[:selected] == "true"
        selected_tmp = true
      else
        selected_tmp = false
      end

      # TODO: グラフの元になる系列｜テーブルに紐付くかを検証すること。
      case p[:series_type]
      when "Series","SeriesOrder"
        series = Series.find(p[:id])
      when "SeriesCalculator"
        series = SeriesCalculator.find(p[:id])
      else
        #raise "Error: series_type=#{p[:series_type]}"
      end

      # seriesがnilになる場合があるのでチェック。
      # TODO: 要確認。チェックボックスのfalse用のhiddenのせい？
      unless series.nil?
        gs = series.graph_series.build({sequence: i, selected: selected_tmp, series_type: p[:series_type]})
        gs.color = p[:color]
        gs.use_val = p[:use_val]
        gs.val_font = p[:val_font]
        gs.val_color = p[:val_color]
        gs.val_y_offset = p[:val_y_offset]
        gs.val_x_offset = p[:val_x_offset]
        self.graph_series << gs
      end
    }
  end

  # 入力された系列の種別（クラス）とIDで該当する設定を検索する。
  def have_series_option?(series)
    self.graph_series.each{|s|
      if s.series_type == series.series_type && s.seriesable_id == series.series_id
        return s
      end
    }
    return nil
  end

  # JSON形式で保持されている系列の表示オプションをパースして取得
  # 保存された設定は、その後で系列が追加・削除されている可能性があるため、実際の系列と同じではない。
  # そのため、本メソッドで設定を取得する際に、まずグラフに紐付く系列の設定を取得し、それに
  # 保存されている設定を上書きすることで、グラフに紐付く全ての設定を取得するようにする。
  def get_all_series_options

=begin
    all_series = Array.new #系列ごとのオプションンを適用するために全系列を取得
    if self.graphable_type == "Series"
      all_series << self.graphable
    elsif self.graphable_type == "SeriesCollection"
      all_series = self.graphable.sources
    end

    # グラフの元になる(系列|系列コレクション)に紐付いた全ての(実系列|計算系列)について配列を作成。
    #selected_series = Array.new
    selected_series = MyLib::MySeriesOptions.new
    all_series.each{|series|
      case series.class.name 
      when "Series","SeriesCalculator"
        series_tmp = MyLib::MySeriesOption.new
        series_tmp.series_class = series.class.name
        series_tmp.series_type = series.class.name
        series_tmp.series_id = series.id
        series_tmp.name = series.name
        selected_series << series_tmp
      when "SeriesOrder"
        # 系列コレクションに紐付く系列の場合、IDは実系列のID、名前はorderに設定されたエイリアス
        # または実系列の名前から取得。
        series_tmp = MyLib::MySeriesOption.new
        series_tmp.series_class = "Series"
        series_tmp.series_type = series.class.name
        series_tmp.series_id = series.series.id
        series_tmp.name = series.name
        selected_series << series_tmp
      end
    }
=end

    # グラフのソースが系列でも系列コレクションでもそれに紐付く
    # 系列｜計算系列等から系列オプションのひな形を作成する。
    series_options = self.graphable.get_all_series_options

    series_options.each{|series|
      # 該当する系列の設定を読み取る
      op = self.have_series_option?(series)
      if op.blank?
        # 該当する設定が無い場合、新しく追加された系列と見なしてデフォルトの設定を作る。
        # TODO デフォルトの設定はfalseとするべきではないか。
        # MEMO グラフを新規作成の場合はすべてtrueになる必要がある。編集の場合はデフォルトはfalse
        #series[:selected]=true
	# 2015-01-20 系列の設定が無い場合はfalseにする。
	# グラフを新規作成の場合は、コントローラーの方で強制的にtrueにする。
        series.selected = false
      else
        #series[:selected]=op.selected
        if self.graphable_type == "Series"
          # グラフの元が単独の系列だった場合は強制的に選択状態
          series.selected = true
        else
          series.selected = op.selected
        end
        series.color = op.color
        series.use_val = op.use_val
        series.val_color = op.val_color
        series.val_font = op.val_font
        series.val_y_offset = op.val_y_offset
        series.val_x_offset = op.val_x_offset
      end
    }

    return series_options
  end

  def chart_area_select
    if self.only_chart == 'no' && self.only_chart_width_title == 'no'
      return 'all'
    end

    if self.only_chart == 'yes' && self.only_chart_width_tytle == 'no'
      return 'only_chart'
    end

    if self.only_chart == 'no' && self.only_chart_width_title == 'yes'
      return 'chart_and_title'
    end

    return 'all'
  end

  # 個別指定された行の選択設定のリストを取得。
  def get_choice_list
    arr = MyLib::MyChoiceList.new

    if self.select_choice.nil?
      return arr
    end

    self.select_choice.each{|v|
      arr.push(v)
    }
    return arr
  end

private
  def convBoolean(val)
    if val == true || val == "true"
      "yes"
    else
      "no"
    end
  end

  def params_to_hash(params)
    if params.class.name == 'ActiveSupport::HashWithIndifferentAccess'
      new_params = Hash.new
      params.each{|key, val|
        new_params[key.to_sym] = params_to_hash(val)
      }
      return new_params
    else
      return params
    end
  end

  # DBのカラムがハッシュで入っていない状態の一時的な回避策
  def set_font_properties
    unless self[:title_font].class.name == "Hash"
      self[:title_font] = {size: 28, font: "san-serif"}
    end
    unless self[:sub_title_font].class.name == "Hash"
      self[:sub_title_font] = {size: 12, font: "san-serif"}
    end
    unless self[:hanrei_font].class.name == "Hash"
      self[:hanrei_font] = {size: 12, font: "san-serif"}
    end
    unless self[:unit_font].class.name == "Hash"
      self[:unit_font] = {size: 12, font: "san-serif"}
    end
    unless self[:x_scale_font].class.name == "Hash"
      self[:x_scale_font] = {size: 12, font: "san-serif"}
    end
    unless self[:y_scale_font].class.name == "Hash"
      self[:y_scale_font] = {size: 12, font: "san-serif"}
    end
  end

end
