# -*- coding: utf-8 -*-
class Value < ActiveRecord::Base
  attr_accessible :category_axis_label_text, :sequence, :series_id, :value_num, :value_text, :category_axis_label_int, :note

  def label

    case self.category_axis_type
      when 'integer' then
        return sprintf("%d", self.category_axis_label_int)
      when 'date_y' then
        return sprintf("%d", self.category_axis_label_int)
      when 'date_ym' then
        my_ym = MyLib::MyYearMonth.new(self.category_axis_label_int)
        return my_ym.to_s
      when 'date_ymd' then
        return self.category_axis_label_datetime.strftime("%Y-%m-%d")
      else
        return self.category_axis_label_text
    end

    return self.category_axis_label_text

  end

  def value
    unless self.value_int.nil?
      return self.value_int.to_i
    end

    unless self.value_float.nil?
      return self.value_float.to_f
    end

    return nil
  end

  # 値を文字列に変換。
  # 小数部を含む場合は末尾のゼロを削除する。
  def value_to_s(*args)

    round_digit = args[0] unless args.size == 0

    case int_or_float?(sprintf("%f", self.value))
    when :int
      return sprintf("%d", self.value)
    when :float
      unless round_digit.nil?
        str_f =  sprintf("%#.#{round_digit}f", self.value.round(round_digit))
        return str_f.gsub(/\.$/, '') # 最後が.で終わっている場合は削除する。
      else
        return float_to_s(sprintf("%f", self.value))
      end
    else
      return ""
    end

  end

  def label=(str)

    str_tmp = str.gsub(/^\s?/,'').gsub(/\s?$/,'')

    validate_label(str_tmp)

    case self.category_axis_type
      when 'integer' then
        self.category_axis_label_int = str_tmp.to_i
      when 'date_y' then
        self.category_axis_label_int = set_date_y_to_label(str_tmp)
      when 'date_ym' then
        self.category_axis_label_int = set_date_ym_to_label(str_tmp)
      when 'date_ymd' then
        self.category_axis_label_datetime = set_date_ymd_to_label(str_tmp)
      else
        self.category_axis_label_text = str_tmp
    end

  end

  def value=(var)
    case int_or_float?(var)
    when :int
      self.value_int = var.to_i
    when :float
      self.value_float = var.to_f
    else
      self.value_int = nil
      self.value_float = nil
    end
  end

  def category_axis_label
    if !self.category_axis_label_text.blank?
      return self.category_axis_label_text
    else !self.category_axis_label_int.blank?
      return sprintf("%d",self.category_axis_label_int)
    end
  end

private
  def set_date_y_to_label(str)
    return str.to_i
  end

  def set_date_ym_to_label(str)
    my_ym = MyLib::MyYearMonth.new(str)
    return my_ym.to_i
  end

  def set_date_ymd_to_label(str)
    return DateTime.parse(str)
  end

  # 与えられた文字列が整数か小数部を含むかを判定。
  def int_or_float?(val)

    if val =~ /^[\+\-]*\d[\d,\,]*\.\d+$/
      return :float
    elsif val =~ /^[\+\-]*\d[\d,\,]*$/
      return :int
    else
      return :error
    end

  end

  # 小数部を含む文字列を整形
  def float_to_s(str)

    str_i, str_f = str.split('.')

    if str_f.blank?
      return str_i
    else
      str_f = str_f.gsub(/0*$/, '')
      if str_f.size > 0
        return str_i + '.' + str_f
      else
        return str_i
      end
    end

    return ""

  end

  # 項目軸ラベルの書式を検証
  def validate_label(str)
#debugger

    msg = "項目軸ラベルの値が不正です。(label='#{str}')" # 共通メッセージ
    fg = false
 
    case self.category_axis_type
      when 'integer' then
        unless /^\-*[\d]+$/.match(str)
	  msg += "\n項目軸ラベルは数値形式が設定されています。"
	  fg = true
        end
      when 'date_y' then
        unless /^\d{4}$/.match(str)
	  msg += "\n項目軸ラベルは日付形式が設定されています。yyyyの形式で入力してください。"
	  fg = true
        end
      when 'date_ym' then
        unless /^\d{4}\-\d{1,2}$/.match(str)
	  msg += "\n項目軸ラベルは日付形式が設定されています。yyyy-mmの形式で入力してください。"
	  fg = true
        end
      when 'date_ymd' then
        unless /^\d{4}\-\d{1,2}$/.match(str)
	  msg += "\n項目軸ラベルは日付形式が設定されています。yyyy-mm-ddの形式で入力してください。"
	  fg = true
        end
    end

    if fg
      ex = MyException::MyParsingError.new(msg)
      raise ex
    end

  end

end
