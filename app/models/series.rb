# -*- coding: utf-8 -*-
class Series < ActiveRecord::Base
  attr_accessible :name, :category_axis_type, :category_axis_unit, :description, :unit, :locked, :round_digit
  attr_protected :id, :user_id, :filter_target_id, :series_filter_id, :is_filter, :series_type

  after_initialize :extend_to_filter
  after_find :extend_to_filter

  belongs_to :user, counter_cache: :series_count
  has_many :values, dependent: :delete_all
  
  # series_collectionsとのn:m関係
  has_many :series_orders
  has_many :series_collections, :through => :series_orders

  # graphとのn:m関係
  has_many :graph_series, :as => :seriesable # ポリモーフィック関連
  has_many :graphs, :through => :seriesable

  has_one :series_filter, dependent: :destroy

  # 同一テーブルでの親子関係
  belongs_to :filter_target, class_name: "Series", foreign_key: :filter_target_id
  has_many   :filters, class_name: "Series", foreign_key: :filter_target_id

  # graphsテーブルとのポリモーフィック関連
  has_many :graphs, :as => :graphable, dependent: :delete_all

  # validation
  validates :category_axis_type, inclusion: { in: MY_APP['category_axis_type_label'].keys }
  validates :description, length: {maximum: 10000}, allow_blank: true

  def to_csv

    csv_text = Array.new
    self.values.each{|value|
     #value_tmp = value.value.blank? ? '' : sprintf("%d",value.value) 
      value_tmp = value.value.blank? ? '' : value.value_to_s(self.round_digit) 
      csv_text.push( [ERB::Util.html_escape(value.label), value_tmp, ERB::Util.html_escape(value.note)].join(',') )
    }

    return csv_text.join("\n")

  end

  # ccchartの元データとなる配列を返す。
  # [['年度',2001,2001..],['系列名',100,200..]]
  def to_json_for_chart

    category_axis = Array.new
    values_tmp = Array.new

    # 1行目の設定
    # 項目列の1行目はX軸の単位。
    # TODO: 今は決め打ちだがDBから設定するようにする。
    if self.category_axis_unit.blank?
      category_axis.push('')
    else
      category_axis.push(self.category_axis_unit)
    end
    # 値列の1行目は系列の名前
    values_tmp.push(ERB::Util.html_escape(self.name))

    self.values.each{|value|
      if value.label.kind_of?(String)
        category_axis.push(ERB::Util.html_escape(value.label))
      else
        category_axis.push(value.label)
      end
      value_tmp = value.value.blank? ? nil : value.value
      values_tmp.push(value_tmp)
    }

    res = Array.new
    res.push(category_axis)
    res.push(values_tmp)
    return res

  end

  # HTMLテーブルのソースとなるデータに整形。
  def series_table
    return self.temporary_table
  end

  # 様々な形式に変換するための一時テーブルを取得する。
  def temporary_table

    temp_table = MyLib::MyTemporaryTableFactory.new

    series_collection = SeriesCollection.new
    temp_table.series_collection = series_collection

    series_order = SeriesOrder.new
    series_order.series_id = self.id
    series_order.sequence  = 0
    temp_table.series_orders = Array.new
    temp_table.series_orders.push(series_order)

    table = temp_table.eval

    return table

  end


  def is_filter_to_str(true_text, false_text)
    if self.is_filter
      return true_text
    else
      return false_text
    end
  end

  def series_type_to_str(*arg)
    if arg.nil?
      return self.series_type
    end

    type_alias = arg[0]
    if self.is_filter
      if type_alias.keys.include?('series_filter')
        return type_alias['series_filter']
      else
        return 'series_filter'
      end
    else
      series_type_tmp = self.series_type.blank? ? 'series' : self.series_type
      if type_alias.keys.include?(series_type_tmp)
        return type_alias[series_type_tmp]
      else
        return series_type_tmp
      end
    end
  end

  def category_axis_is_sortable?
    ['date_y', 'date_ym', 'date_ymd', 'integer'].include?(self.category_axis_type)
  end

  def category_axis_is_date?
    ['date_y', 'date_ym', 'date_ymd'].include?(self.category_axis_type)
  end

  def category_axis_type_label
    MY_APP['category_axis_type_label'][self.category_axis_type]
  end

  # フィルタの場合、元の系列のグラフをさかのぼってすべて返す。
  def child_graphs
    graphs_tmp = Array.new
    if is_filter
      self.filter_target.graphs.each{|graph|
        # 重複したグラフは排除する
        duplicate_fg = false
        graphs_tmp.each{|g|
          if g.id == graph.id
            duplicate_fg = true
          end
        }
        if !duplicate_fg
          graphs_tmp.push(graph)
        end
      }
      #graphs_tmp += self.filter_target.child_graphs
    end
    return graphs_tmp
  end

  # 全ての系列の系列オプションを取得する。
  # seriesの場合は必然的に系列は1つだけ。
  def get_all_series_options

    series_options = MyLib::MySeriesOptions.new
    series_tmp = MyLib::MySeriesOption.new
    series_tmp.series_class = self.class.name
    series_tmp.series_type = self.class.name
    series_tmp.series_id = self.id
    series_tmp.name = self.name
    series_tmp.color_template = CHART_CONFIG[:colorSet][0]
    series_options << series_tmp

    return series_options

  end

  # テキストから値を作成する。（同時に保存する）
  def create_values_by_text(input_values, delimitter_type='comma')
    vs = build_values_by_text(input_values, delimitter_type)
    vs.each{|v|
      v.save!
    }
    return vs
  end

  # テキストからvaluesオブジェクトを作成する。（保存はしない）
  def build_values_by_text(input_values, delimitter_type='comma')
    res = Array.new
    delimitter = {'comma' => ',', 'tab' => "\t"}[delimitter_type]
    input_values.split("\n").each_with_index{|line, i|
      label,value,note = line.split(delimitter)
      value_tmp = Value.new
      value_tmp.category_axis_type = self.category_axis_type
      value_tmp.series_id = self.id
      value_tmp.sequence = i+1
      value_tmp.label = label
      value_tmp.value = value.gsub(/\r/, '').gsub(/\s/,'').gsub(/\,/,'') unless value.nil? #デリミタを間違えるとvalueがnilの場合もある。
      value_tmp.note = note
      res.push(value_tmp)
    }

    # ラベルの重複をチェック
    value_tmp = nil
    res.sort{|a,b| a.label <=> b.label }.each{|v|
      if !value_tmp.nil? && v.label == value_tmp.label
        raise MyException::MyParsingError.new("項目軸ラベルが重複しています。(label=#{v.label})")
      end
      value_tmp = v
    }
    return res
  end

  # クラスメソッド
  class << self

    def find_all_available
      return Series.find(:all, :conditions => ["deleted = ?", false])
    end

    def find_available_with_user_id(user_id)
      return Series.find(:all, :conditions => ["user_id = ? and deleted = ?", user_id, false])
    end

  end

protected

  # 初期化時および検索時に、フィルター系列の場合には
  # 拡張モジュールを読み込む。
  def extend_to_filter
    if self.is_filter
      self.extend(MyLib::MyFilterExtension)
      @values_cache = Array.new
      @series_origin = Series.find(filter_target_id)
      @ref_value = self.get_reference_value
      filtering_values
    end
  end

end
