class SeriesOrder < ActiveRecord::Base
  attr_accessible :sequence, :series_collection_id, :series_id, :alias

  belongs_to :series_collection
  belongs_to :series

=begin
  def name_or_alias
    self.alias.blank? ? self.series.name : self.alias
  end
=end

  def name
    self.alias.blank? ? self.series_with_deleted.name : self.alias
  end

  def series_with_deleted
    series_tmp = self.series
    if series_tmp.nil?
      series_tmp = Series.new
      series_tmp.id = self.series_id
      series_tmp.name = 'Unknown'
    end
    return series_tmp
  end

  def series_is_deleted?
    self.series.nil?
  end

end
