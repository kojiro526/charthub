class CcchartMemo < ActiveRecord::Base
  attr_accessible :val, :top, :left, :color, :font, :line_to, :line_to_color, :line_to_x_offset, :line_space
  serialize :font, Hash

  belongs_to :graph

  def attributes_from_params=(params)
    pm = Hash.new
    params.each{|key, val|
      hash_tmp = Hash.new
      case key
      when 'font'
        # font系の設定を保存用に変換
        hash_tmp[key] = params_to_hash(val)
      else
        hash_tmp[key] = val
      end

      hash_tmp.each{|k, v|
        pm[k] = v
      }
    }

    self.attributes = pm
  end

  def params_to_hash(params)
    if params.class.name == 'ActiveSupport::HashWithIndifferentAccess'
      new_params = Hash.new
      params.each{|key, val|
        new_params[key.to_sym] = params_to_hash(val)
      }
      return new_params
    else
      return params
    end
  end

  def to_config
    memo_arr = Array.new
    memo_lines = self.val.gsub(/\r\n/, "\n").gsub(/\r/, "\n").split("\n")
    self.line_space ||= 4
    memo_lines.each_with_index{|line, i|
      arr = Hash.new
      arr['val'] = line
      arr['left'] = self.left
      arr['top'] = self.top + self.font[:size].to_i + (self.font[:size].to_i + self.line_space) * i
      arr['color'] = self.color unless self.color.blank?
      #arr['font'] = "100 #{self.font[:size]}px #{self.font[:font]}"
      arr['font'] = "200 #{self.font[:size]}px 'Noto Sans Japanese'"
      arr['lineTo'] = self.line_to  unless self.line_to.blank?
      arr['lineToColor'] = self.line_to_color unless self.line_to_color.blank?
      arr['lineToXOffset'] = self.line_to_x_offset unless self.line_to_x_offset.blank?
      memo_arr.push(arr)
    }
    return memo_arr
  end
end
