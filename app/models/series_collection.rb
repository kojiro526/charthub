# -*- coding: utf-8 -*-
class SeriesCollection < ActiveRecord::Base
  attr_accessible :description, :name, :user_id, :locked

  has_many :series_orders, dependent: :delete_all
  has_many :series, :through => :series_orders
  belongs_to :user, counter_cache: :series_collections_count

  has_many :graphs, :as => :graphable, dependent: :delete_all

  has_many :series_calculators, dependent: :delete_all

  def to_json_for_chart
    return self.series_table.to_json_for_chart
  end

  def to_json_for_chart_with_option(op)
    return self.series_table.to_json_for_chart_with_option(op)
  end

  # HTMLテーブルのソースとなるデータに整形。
  def series_table
    return self.temporary_table
  end

  def series_table_with_view_options(op)
    return self.temporary_table_with_view_options(op)
  end

  # 様々な形式に変換するための一時テーブル
  def temporary_table

    temp_table = MyLib::MyTemporaryTableFactory.new
    temp_table.series_collection = self
    temp_table.series_orders = self.series_orders
    temp_table.series_calculators = self.series_calculators
    return temp_table.eval

    #return temp_table

  end

  def temporary_table_with_view_options(op)

    temp_table = MyLib::MyTemporaryTableFactory.new
    temp_table.series_collection = self
    temp_table.series_orders = self.series_orders
    temp_table.series_calculators = self.series_calculators
    temp_table.view_options = op
    return temp_table.eval

    #return temp_table

  end

  # 実系列と計算系列のソースを合わせた配列を作成する。
  def sources

    sources = Array.new

    # 実系列のヘッダー情報を収集する。
    self.series_orders.each{|order|
      sources << order
    }
    # 計算系列のヘッダー情報を収集する。
    self.series_calculators.each{|culc|
      sources << culc
    }

    sources.sort!{|a, b|
      a.sequence <=> b.sequence
    }

    return sources

  end

  # 紐付く系列のグラフを取得
  def child_graphs
    graphs_tmp = Array.new
    self.series_orders.each{|order|
      # テーブルに含まれる系列自身に紐づくグラフを取得
      order.series_with_deleted.graphs.each{|graph|
        # 重複したグラフは排除する
        duplicate_fg = false
        graphs_tmp.each{|g|
          if g.id == graph.id
            duplicate_fg = true
          end
        }
        if !duplicate_fg
          graphs_tmp.push(graph)
        end
      }

      # テーブルに含まれる系列の先祖系列に紐づくグラフを取得
      #graphs_tmp +=  order.series_with_deleted.child_graphs
      order.series_with_deleted.child_graphs.each{|graph|
        # 重複したグラフは排除する
        duplicate_fg = false
        graphs_tmp.each{|g|
          if g.id == graph.id
            duplicate_fg = true
          end
        }
        if !duplicate_fg
          graphs_tmp.push(graph)
        end
      }

    }
    return graphs_tmp
  end

  # 系列コレクションに紐付いた全ての(実系列|計算系列)についてグラフ用オプションの配列を作成。
  # グラフを新規作成する際はこれが空の設定となり、グラフを編集する際にはここで生成した
  # オプションを保存した設定で上書きする。
  def get_all_series_options

    all_series = self.sources

    series_options = MyLib::MySeriesOptions.new
    i=0
    all_series.each{|series|
      case series.class.name 
      when "Series","SeriesCalculator"
        # MEMO: "Series"はseries_collectionの中では発生しないはず。
        #       （graphモデルの中で処理していたときの名残）
        series_tmp = MyLib::MySeriesOption.new
        series_tmp.series_class = series.class.name
        series_tmp.series_type = series.class.name
        series_tmp.series_id = series.id
        series_tmp.name = series.name
      when "SeriesOrder"
        # 系列コレクションに紐付く系列の場合、IDは実系列のID、名前はorderに設定されたエイリアス
        # または実系列の名前から取得。
        series_tmp = MyLib::MySeriesOption.new
        series_tmp.series_class = "Series"
        series_tmp.series_type = series.class.name
        series_tmp.series_id = series.series.id
        series_tmp.name = series.name
      end
      series_tmp.color_template = CHART_CONFIG[:colorSet][i]
      series_options << series_tmp
      i+=1
      i=0 if i >= CHART_CONFIG[:colorSet].size
      
    }

    return series_options

  end

  # 項目軸の種別を返す。
  # 紐付く系列は全て同じ項目軸種別なので、最初の系列のそれを返す。
  # まだ系列が一つも紐付けられていない状態もありうるため、その場合は
  # nilを返す。
  def category_axis_type
    if self.series_orders[0].nil?
      return nil
    else
      return self.series_orders[0].series.category_axis_type
    end
  end

  def category_axis_is_sortable?
    ['date_y', 'date_ym', 'date_ymd', 'integer'].include?(self.category_axis_type)
  end

  def category_axis_is_date?
    ['date_y', 'date_ym', 'date_ymd'].include?(self.category_axis_type)
  end

  def category_axis_type_label
    MY_APP['category_axis_type_label'][self.category_axis_type]
  end

end
