class SeriesFilter < ActiveRecord::Base
  attr_accessible :child_series_id, :description, :filter_expression, :filter_type, :name, :reference_value, :series_id, :user_id

#  after_initialize :initialize_class_values, :initialize_values
#  after_find :initialize_class_values, :initialize_values

  belongs_to :series
#  has_and_belongs_to_many :series_collections

=begin
  def values(*args)

    if args[0] == true
      initialize_values
    end

    return @values

  end
=end

  # クラスメソッド
  # TODO: deletedメソッドを追加したら修正。
=begin
  class << self

    def find_available_with_user_id(user_id)
      return SeriesFilter.find(:all, :conditions => ["user_id = ?", user_id])
    end

  end
=end

private
=begin

  # 変数を初期化
  def initialize_class_values
    @series = nil
    @values = Array.new
  end

  # 子である系列または系列フィルタを読み込む。
  # newまたはfindされた時に実行する。
  def initialize_values

    if !series_id.blank?
      @series = Series.find(series_id, :include => :values)
    elsif !child_series_id.blank?

    end

    # 値を更新する。
    filtering_value

  end

  # 参照する値が設定されている場合、その値を取得する。
  def get_reference_value
    ref_value = nil
    @series.values.each{|value|
      if value.label == reference_value
        ref_value = value.value
      end
    }
    return ref_value
  end

  # 値を変換する。
  # フィルタの種類に応じて処理を変える。
  def value_filter(value)
    if filter_type == 'relativezation'
      return value.to_f / @ref_value.to_f
    else

    end
  end

  # 値を変換して保存する。
  def filtering_value

    @ref_value = get_reference_value
    @values = Array.new

    @series.values.each{|value|
      value_tmp = MyLib::MyValue.new
      value_tmp.label = value.label
      value_tmp.value = value_filter(value.value)
      @values.push(value_tmp)
    }

  end
=end
end
