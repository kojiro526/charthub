class GraphSeries < ActiveRecord::Base
  attr_accessible :color, :graph_id, :sequence, :seriesable_id, :seriesable_type, :use_val, :val_color, :val_font, :val_x_offset, :val_y_offset, :selected, :series_type

  serialize :val_font, Hash

  belongs_to :graph
  belongs_to :seriesable, :polymorphic => true

  def to_series_option
    s = MyLib::MySeriesOption.new
    s.series_id = self.seriesable_id
    s.series_class = self.seriesable_type
    s.series_type = self.series_type
    s.selected = self.selected
    return s
  end
end
