class SeriesCalculator < ActiveRecord::Base
  attr_accessible :expression, :name, :series_collection_id, :sequence

  belongs_to :series_collection

  # graphとのn:m関係
  has_many :graph_series, :as => :seriesable # ポリモーフィック関連
  has_many :graphs, :through => :graph_series

  def eval(table)

    p = MyLib::MyEval::MyParser.new
    n = p.parse(self.expression)
    n.set_variable_table(table)
    res = n.eval
    return res

  end
end
