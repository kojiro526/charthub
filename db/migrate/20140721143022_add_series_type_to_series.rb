class AddSeriesTypeToSeries < ActiveRecord::Migration
  def change
    add_column :series, :series_type, :string
  end
end
