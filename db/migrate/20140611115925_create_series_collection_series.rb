class CreateSeriesCollectionSeries < ActiveRecord::Migration
  def up
    create_table :series_collections_series, :force => true do |t|
      t.integer :series_collection_id
      t.integer :series_id
    end
  end

  def down
    drop_table :series_collections_series
  end
end
