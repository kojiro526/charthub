class AddSelectedToGraphSeries < ActiveRecord::Migration
  def change
    add_column :graph_series, :selected, :boolean, :nil => false, :default => false
  end
end
