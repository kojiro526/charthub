class ChangeHanreiFontForGraphs < ActiveRecord::Migration
  def up
    rename_column :graphs, :hanreiFont, :hanrei_font
  end

  def down
    rename_column :graphs, :hanrei_font, :hanreiFont
  end
end
