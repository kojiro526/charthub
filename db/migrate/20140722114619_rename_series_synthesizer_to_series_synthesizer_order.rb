class RenameSeriesSynthesizerToSeriesSynthesizerOrder < ActiveRecord::Migration
  def up
    rename_table :series_synthesizers, :series_synthesizer_orders
    rename_column :series_synthesizer_orders, :series_id, :series_synthesizer_id
    rename_column :series_synthesizer_orders, :target_series_id, :series_id
  end

  def down
    rename_column :series_synthesizer_orders, :series_id, :target_series_id 
    rename_column :series_synthesizer_orders, :series_synthesizer_id, :series_id 
    rename_table :series_synthesizer_orders, :series_synthesizers 
  end

end
