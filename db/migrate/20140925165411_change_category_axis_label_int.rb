class ChangeCategoryAxisLabelInt < ActiveRecord::Migration
  def up
    change_column :values, :category_axis_label_int, :integer
  end

  def down
    change_column :values, :category_axis_label_int, :string
  end
end
