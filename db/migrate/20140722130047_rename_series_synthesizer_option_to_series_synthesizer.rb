class RenameSeriesSynthesizerOptionToSeriesSynthesizer < ActiveRecord::Migration
  def up
    rename_table :series_synthesizer_options, :series_synthesizers
  end

  def down
    rename_table :series_synthesizers, :series_synthesizer_options 
  end   
end
