class ModifyUnitForSeries < ActiveRecord::Migration
  def up
    change_column :series, :unit, :string, :null => true
    change_column :series_filters, :name, :string, :null => true
    change_column :series_filters, :user_id, :integer, :null => true
  end

  def down
    change_column :series, :unit, :string, :null => false
    change_column :series_filters, :name, :string, :null => false
    change_column :series_filters, :user_id, :integer, :null => false
  end
end
