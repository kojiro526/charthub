class CreateSeriesCollectionSeriesFilters < ActiveRecord::Migration
  def up
    create_table :series_collections_series_filters, :force => true do |t|
      t.integer :series_collection_id
      t.integer :series_filter_id
    end
  end

  def down
    drop_table :series_collections_series_filters
  end
end
