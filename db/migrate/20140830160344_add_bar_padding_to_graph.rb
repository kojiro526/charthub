class AddBarPaddingToGraph < ActiveRecord::Migration
  def change
    add_column :graphs, :bar_padding, :integer
  end
end
