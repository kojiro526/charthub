class DeleteUnusedColumnsFromSeriesFilter < ActiveRecord::Migration
  def up
    remove_column :series, :series_filter_id
    remove_column :series_filters, :user_id
    remove_column :series_filters, :name
    remove_column :series_filters, :child_series_id
  end

  def down
    add_column :series, :series_filter_id, :integer
    add_column :series_filters, :user_id, :integer
    add_column :series_filters, :name, :string
    add_column :series_filters, :child_series_id, :integer
  end
end
