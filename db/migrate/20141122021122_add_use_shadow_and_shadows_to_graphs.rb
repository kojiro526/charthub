class AddUseShadowAndShadowsToGraphs < ActiveRecord::Migration
  def change
    add_column :graphs, :use_shadow, :boolean, :default => true
    add_column :graphs, :shadows, :string
  end
end
