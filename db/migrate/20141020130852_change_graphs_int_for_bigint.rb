class ChangeGraphsIntForBigint < ActiveRecord::Migration
  def up
    change_column :graphs, :max_y, :bigint
    change_column :graphs, :min_y, :bigint
    change_column :graphs, :max_x, :bigint
    change_column :graphs, :min_x, :bigint
  end

  def down
    change_column :graphs, :max_y, :integer
    change_column :graphs, :min_y, :integer
    change_column :graphs, :max_x, :integer
    change_column :graphs, :min_x, :integer
  end
end
