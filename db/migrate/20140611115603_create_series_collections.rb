class CreateSeriesCollections < ActiveRecord::Migration
  def change
    create_table :series_collections do |t|
      t.integer :user_id
      t.string :name, :null => false
      t.text :description

      t.timestamps
    end
  end
end
