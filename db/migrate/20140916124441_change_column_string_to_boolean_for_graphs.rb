class ChangeColumnStringToBooleanForGraphs < ActiveRecord::Migration
  def up
    change_column :graphs, :use_val, :boolean, :default => 0, :null => :false
    change_column :graphs, :percent_val, :boolean, :default => 0, :null => :false
    change_column :graphs, :use_tool_tip, :boolean, :default => 0, :null => :false
    change_column :graphs, :y_scale_percent, :boolean, :default => 0, :null => :false
  end

  def down
    change_column :graphs, :use_val, :string
    change_column :graphs, :percent_val, :string
    change_column :graphs, :use_tool_tip, :string
    change_column :graphs, :y_scale_percent, :string
  end
end
