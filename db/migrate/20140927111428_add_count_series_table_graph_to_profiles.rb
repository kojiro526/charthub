class AddCountSeriesTableGraphToProfiles < ActiveRecord::Migration
  def change
    add_column :users, :series_count, :integer, default: 0
    add_column :users, :series_collections_count, :integer, default: 0
    add_column :users, :graphs_count, :integer, default: 0
  end
end
