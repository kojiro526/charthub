class ChangeTopAndLeftForCcchartMemos < ActiveRecord::Migration
  def up
    change_column :ccchart_memos, :top, :integer, default: 20
    change_column :ccchart_memos, :left, :integer,  default: 20
  end

  def down
    change_column :ccchart_memos, :top, :integer, default: 0
    change_column :ccchart_memos, :left, :integer, default: 0
  end
end
