class AddRoundDigitToGraph < ActiveRecord::Migration
  def change
    add_column :graphs, :round_digit, :integer
  end
end
