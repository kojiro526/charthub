class AddOpenToSeriesCollection < ActiveRecord::Migration
  def change
    add_column :series_collections, :locked, :boolean, :nil => false, :default => false
  end
end
