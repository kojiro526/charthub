class RenameSeriesCulcuratorToSeriesCalculator < ActiveRecord::Migration
  def up
    rename_table :series_culcurators, :series_calculators
  end

  def down
    rename_table :series_calculators, :series_culcurators
  end
end
