class AddBarWidthToGraphs < ActiveRecord::Migration
  def change
    add_column :graphs, :bar_width, :integer
  end
end
