class DropTableSeriesCollectionSeries < ActiveRecord::Migration
  def up
    drop_table :series_collections_series
    drop_table :series_collections_series_filters
  end

  def down
  end
end
