class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.integer :user_id, :null => false
      t.string :name
      t.text :introduction

      t.timestamps
    end
  end
end
