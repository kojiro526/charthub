class AddPieConfigToGraphs < ActiveRecord::Migration
  def change
    add_column :graphs, :pie_data_index, :integer
    add_column :graphs, :col_name_font, :string
    add_column :graphs, :pie_ring_width, :integer
    add_column :graphs, :pie_hole_radius, :integer
    add_column :graphs, :text_color, :string
  end
end
