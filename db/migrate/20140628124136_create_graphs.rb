class CreateGraphs < ActiveRecord::Migration
  def change
    create_table :graphs do |t|
      t.integer :user_id, :null => false
      t.string  :graphable_type, :null => false
      t.integer :graphable_id, :null => false
      t.string  :chart_type
      t.integer :width
      t.integer :height
      t.integer :axis_x_width
      t.integer :axis_x_len
      t.integer :axis_y_width
      t.integer :axis_y_len
      t.string :only_chart
      t.string :only_chart_width_title
      t.integer :padding_top
      t.integer :padding_bottom
      t.integer :padding_left
      t.integer :padding_right
      t.string :x_color
      t.string :y_color
      t.integer :y_scale_skip
      t.integer :x_scale_rotate
      t.integer :y_scale_rotate
      t.string :title
      t.string :title_color
      t.string :title_font
      t.string :title_text_align
      t.integer :title_y
      t.string :sub_title
      t.string :sub_title_color
      t.string :sub_title_font
      t.string :sub_title_text_align
      t.integer :sub_title_y
      t.string :x_scale_color
      t.string :x_scale_font
      t.string :x_scale_align
      t.integer :x_scale_x_offset
      t.integer :x_scale_y_offset
      t.integer :x_scale_skip
      t.integer :col_names_title_offset
      t.string :y_scale_color
      t.string :y_scale_font
      t.string :y_scale_align
      t.integer :y_scale_x_offset
      t.integer :y_scale_y_offset
      t.string :y_scale_percent
      t.string :hanrei_color
      t.string :hanreiFont
      t.string :hanrei_align
      t.string :hanrei_line_height
      t.integer :hanrei_x_offset
      t.integer :hanrei_y_offset
      t.integer :hanrei_radius
      t.string :hanrei_marker_style
      t.string :unit
      t.string :unit_color
      t.string :unit_font
      t.string :unit_align
      t.integer :unit_x_offset
      t.integer :unit_y_offset
      t.string :use_val
      t.string :percent_val
      t.string :use_tool_tip
      t.string :bar_tip_anchor_color
      t.string :use_marker
      t.boolean :use_first_to_col_name
      t.boolean :use_first_to_row_name
      t.integer :max_y
      t.integer :min_y
      t.integer :rounded_up_max_y
      t.integer :max_x
      t.integer :min_x
      t.integer :rounded_up_max_x
      t.string :color_set
      t.integer :line_width
      t.integer :border_width
      t.integer :marker_width
      t.string :bg_gradient
      t.binary :image, :limit => 1.megabyte
      t.binary :thumbnail, :limit => 1.megabyte

      t.timestamps
    end
  end
end
