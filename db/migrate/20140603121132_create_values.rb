class CreateValues < ActiveRecord::Migration
  def change
    create_table :values do |t|
      t.integer :series_id, :null => false
      t.integer :sequence,  :null => false, :default => 0
      t.integer :value_int, :null => true
      t.decimal :value_float, :null => true
      t.string :category_axis_label_text, :null => true
      t.string :category_axis_label_int,  :null => true

      t.timestamps
    end
  end
end
