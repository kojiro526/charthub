class AddCategoryAxisLabelDatetimeToValues < ActiveRecord::Migration
  def change
    add_column :values, :category_axis_label_datetime, :datetime
  end
end
