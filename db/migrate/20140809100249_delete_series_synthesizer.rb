class DeleteSeriesSynthesizer < ActiveRecord::Migration
  def change
    drop_table :series_synthesizers
    drop_table :series_synthesizer_orders
  end
end
