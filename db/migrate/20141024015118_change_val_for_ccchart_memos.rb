class ChangeValForCcchartMemos < ActiveRecord::Migration
  def up
    change_column :ccchart_memos, :val, :text, :null => true
  end

  def down
    change_column :ccchart_memos, :val, :text, :null => false
  end
end
