class ChangeDescriptionForSeries < ActiveRecord::Migration
  def up
    change_column :series, :description, :text
  end

  def down
    change_column :series, :description, :string
  end
end
