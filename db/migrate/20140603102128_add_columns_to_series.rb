class AddColumnsToSeries < ActiveRecord::Migration
  def change
    add_column :series, :unit, :string, :null => false
    add_column :series, :category_axis_type, :string, :null => false, :default => 'none'
    add_column :series, :description, :string, :null => true
  end
end
