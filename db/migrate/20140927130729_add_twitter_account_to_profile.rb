class AddTwitterAccountToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :twitter_account, :string
    add_column :profiles, :include_twitter_account, :boolean, default: false
  end
end
