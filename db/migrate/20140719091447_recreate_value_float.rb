class RecreateValueFloat < ActiveRecord::Migration
  def up
    remove_column :values, :value_float
    add_column :values, :value_float, :decimal, :null => true, :precision => 24, :scale => 8
  end

  def down
    remove_column :values, :value_float
    add_column :values, :value_float, :float, :null => true 
  end
end
