class CreateSeriesOrders < ActiveRecord::Migration
  def change
    create_table :series_orders do |t|
      t.integer :series_collection_id
      t.integer :series_id
      t.integer :sequence, :default => 0, :null => false

      t.timestamps
    end
  end
end
