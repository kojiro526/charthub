class CreateSeriesFilters < ActiveRecord::Migration
  def change
    create_table :series_filters do |t|
      t.integer :user_id, :null => false
      t.string :name, :null => false
      t.string :filter_type, :null => false
      t.text :description
      t.integer :child_series_id, :null => true
      t.integer :series_id, :null => true
      t.string :reference_value, :null => false, :default => ""
      t.string :filter_expression, :null => false, :default => ""

      t.timestamps
    end
  end
end
