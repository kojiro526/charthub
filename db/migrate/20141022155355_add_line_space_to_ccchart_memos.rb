class AddLineSpaceToCcchartMemos < ActiveRecord::Migration
  def change
    add_column :ccchart_memos, :line_space, :integer
  end
end
