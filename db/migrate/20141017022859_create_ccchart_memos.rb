class CreateCcchartMemos < ActiveRecord::Migration
  def change
    create_table :ccchart_memos do |t|

      t.integer :graph_id
      t.string :val, null: false
      t.integer :top, default: 0, null: false
      t.integer :left, default: 0, null: false
      t.string :color
      t.string :font
      t.string :line_to
      t.string :line_to_color
      t.integer :line_to_x_offset

      t.timestamps
    end
  end
end
