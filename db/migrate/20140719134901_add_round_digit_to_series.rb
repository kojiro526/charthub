class AddRoundDigitToSeries < ActiveRecord::Migration
  def change
    add_column :series, :round_digit, :integer, :default => 8, :null => false
  end
end
