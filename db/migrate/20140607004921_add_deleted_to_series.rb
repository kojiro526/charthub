class AddDeletedToSeries < ActiveRecord::Migration
  def change
    add_column :series, :deleted, :boolean, :default => false, :null => false
    add_column :series, :deleted_at, :timestamp, :null => true
  end
end
