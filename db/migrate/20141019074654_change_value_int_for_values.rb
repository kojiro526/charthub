class ChangeValueIntForValues < ActiveRecord::Migration
  def up
    change_column :values, :value_int, :bigint
  end

  def down
    change_column :values, :value_int, :integer
  end
end
