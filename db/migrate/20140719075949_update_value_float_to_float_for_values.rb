class UpdateValueFloatToFloatForValues < ActiveRecord::Migration
  def up
    change_column :values, :value_float, :float, :null => true 
  end

  def down
    change_column :values, :value_float, :decimal ,:null => true
  end
end
