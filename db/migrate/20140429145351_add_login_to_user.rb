class AddLoginToUser < ActiveRecord::Migration
  def change

    add_column :users, :login, :string, :null => true, :limit => 255
    add_index :users, :login, :unique => true 

  end
end
