class CreateSeriesSynthesizers < ActiveRecord::Migration
  def change
    create_table :series_synthesizers do |t|
      t.integer :series_id
      t.integer :target_series_id
      t.integer :sequence
      t.string :operation

      t.timestamps
    end
  end
end
