class CreateSeriesCulcurators < ActiveRecord::Migration
  def change
    create_table :series_culcurators do |t|
      t.integer :series_collection_id, :null => false
      t.integer :sequence, :default => 0, :null => false
      t.string :name
      t.string :expression

      t.timestamps
    end
  end
end
