class CreateSeriesSynthesizerOptions < ActiveRecord::Migration
  def change
    create_table :series_synthesizer_options do |t|
      t.integer :series_id
      t.string :operation

      t.timestamps
    end
  end
end
