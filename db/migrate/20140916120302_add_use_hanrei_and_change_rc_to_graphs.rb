class AddUseHanreiAndChangeRcToGraphs < ActiveRecord::Migration
  def change
    add_column :graphs, :use_hanrei, :boolean, :default => true
    add_column :graphs, :change_r_c, :boolean, :default => false
  end
end
