class AddRangeAndChoiceToGraphs < ActiveRecord::Migration
  def change
    add_column :graphs, :value_select_mode, :integer, :default => 0, :null => false
    add_column :graphs, :select_from, :string
    add_column :graphs, :select_to, :string
    add_column :graphs, :select_choice, :text
  end
end
