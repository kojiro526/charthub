class CreateGraphSeries < ActiveRecord::Migration
  def change
    create_table :graph_series do |t|
      t.integer :graph_id
      t.string :seriesable_type
      t.integer :seriesable_id
      t.integer :sequence, :default => 0, :null => false
      t.string :color
      t.boolean :use_val
      t.string :val_font
      t.string :val_color
      t.integer :val_y_offset
      t.integer :val_x_offset

      t.timestamps
    end
  end
end
