class AddCategoryAxisTypeToValues < ActiveRecord::Migration
  def change
    add_column :values, :category_axis_type, :string, :null => false, :default => 'none'
  end
end
