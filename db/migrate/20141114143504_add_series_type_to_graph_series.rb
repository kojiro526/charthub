class AddSeriesTypeToGraphSeries < ActiveRecord::Migration
  def change
    add_column :graph_series, :series_type, :string
  end
end
