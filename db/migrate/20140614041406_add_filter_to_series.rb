class AddFilterToSeries < ActiveRecord::Migration
  def change
    add_column :series, :is_filter, :boolean, :null => false, :default => false
    add_column :series, :series_filter_id, :integer, :null => true
    add_column :series, :filter_target_id, :integer, :null => true
  end
end
