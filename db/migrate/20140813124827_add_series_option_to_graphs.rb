class AddSeriesOptionToGraphs < ActiveRecord::Migration
  def change
    add_column :graphs, :series_option, :text
  end
end
