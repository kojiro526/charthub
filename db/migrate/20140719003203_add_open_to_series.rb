class AddOpenToSeries < ActiveRecord::Migration
  def change
    add_column :series, :locked, :boolean, :nil => false, :default => false
  end
end
