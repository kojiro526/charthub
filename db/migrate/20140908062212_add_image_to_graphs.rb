class AddImageToGraphs < ActiveRecord::Migration
  def change
    add_column :graphs, :graph_image, :binary, limit: 5.megabyte
    add_column :graphs, :graph_thumbnail, :binary, limit: 1.megabyte
  end
end
