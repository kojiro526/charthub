class ChangeValToTextForCchartMemos < ActiveRecord::Migration
  def up
    change_column :ccchart_memos, :val, :text
  end

  def down
    change_column :ccchart_memos, :val, :string
  end
end
