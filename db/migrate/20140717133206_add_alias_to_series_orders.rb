class AddAliasToSeriesOrders < ActiveRecord::Migration
  def change
    add_column :series_orders, :alias, :string
  end
end
