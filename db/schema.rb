# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20150120134656) do

  create_table "ccchart_memos", :force => true do |t|
    t.integer  "graph_id"
    t.text     "val"
    t.integer  "top",              :default => 20, :null => false
    t.integer  "left",             :default => 20, :null => false
    t.string   "color"
    t.string   "font"
    t.string   "line_to"
    t.string   "line_to_color"
    t.integer  "line_to_x_offset"
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
    t.integer  "line_space"
  end

  create_table "graph_series", :force => true do |t|
    t.integer  "graph_id"
    t.string   "seriesable_type"
    t.integer  "seriesable_id"
    t.integer  "sequence",        :default => 0,     :null => false
    t.string   "color"
    t.boolean  "use_val"
    t.string   "val_font"
    t.string   "val_color"
    t.integer  "val_y_offset"
    t.integer  "val_x_offset"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
    t.boolean  "selected",        :default => false
    t.string   "series_type"
  end

  create_table "graphs", :force => true do |t|
    t.integer  "user_id",                                                       :null => false
    t.string   "graphable_type",                                                :null => false
    t.integer  "graphable_id",                                                  :null => false
    t.string   "chart_type"
    t.integer  "width"
    t.integer  "height"
    t.integer  "axis_x_width"
    t.integer  "axis_x_len"
    t.integer  "axis_y_width"
    t.integer  "axis_y_len"
    t.string   "only_chart"
    t.string   "only_chart_width_title"
    t.integer  "padding_top"
    t.integer  "padding_bottom"
    t.integer  "padding_left"
    t.integer  "padding_right"
    t.string   "x_color"
    t.string   "y_color"
    t.integer  "y_scale_skip"
    t.integer  "x_scale_rotate"
    t.integer  "y_scale_rotate"
    t.string   "title"
    t.string   "title_color"
    t.string   "title_font"
    t.string   "title_text_align"
    t.integer  "title_y"
    t.string   "sub_title"
    t.string   "sub_title_color"
    t.string   "sub_title_font"
    t.string   "sub_title_text_align"
    t.integer  "sub_title_y"
    t.string   "x_scale_color"
    t.string   "x_scale_font"
    t.string   "x_scale_align"
    t.integer  "x_scale_x_offset"
    t.integer  "x_scale_y_offset"
    t.integer  "x_scale_skip"
    t.integer  "col_names_title_offset"
    t.string   "y_scale_color"
    t.string   "y_scale_font"
    t.string   "y_scale_align"
    t.integer  "y_scale_x_offset"
    t.integer  "y_scale_y_offset"
    t.boolean  "y_scale_percent",                            :default => false
    t.string   "hanrei_color"
    t.string   "hanrei_font"
    t.string   "hanrei_align"
    t.string   "hanrei_line_height"
    t.integer  "hanrei_x_offset"
    t.integer  "hanrei_y_offset"
    t.integer  "hanrei_radius"
    t.string   "hanrei_marker_style"
    t.string   "unit"
    t.string   "unit_color"
    t.string   "unit_font"
    t.string   "unit_align"
    t.integer  "unit_x_offset"
    t.integer  "unit_y_offset"
    t.boolean  "use_val",                                    :default => false
    t.boolean  "percent_val",                                :default => false
    t.boolean  "use_tool_tip",                               :default => false
    t.string   "bar_tip_anchor_color"
    t.string   "use_marker"
    t.boolean  "use_first_to_col_name"
    t.boolean  "use_first_to_row_name"
    t.integer  "max_y",                  :limit => 8
    t.integer  "min_y",                  :limit => 8
    t.integer  "rounded_up_max_y"
    t.integer  "max_x",                  :limit => 8
    t.integer  "min_x",                  :limit => 8
    t.integer  "rounded_up_max_x"
    t.string   "color_set"
    t.integer  "line_width"
    t.integer  "border_width"
    t.integer  "marker_width"
    t.string   "bg_gradient"
    t.binary   "image",                  :limit => 16777215
    t.binary   "thumbnail",              :limit => 16777215
    t.datetime "created_at",                                                    :null => false
    t.datetime "updated_at",                                                    :null => false
    t.integer  "round_digit"
    t.text     "series_option"
    t.integer  "bar_padding"
    t.binary   "graph_image",            :limit => 16777215
    t.binary   "graph_thumbnail",        :limit => 16777215
    t.string   "bg"
    t.boolean  "use_hanrei",                                 :default => true
    t.boolean  "change_r_c",                                 :default => false
    t.integer  "bar_width"
    t.integer  "pie_data_index"
    t.string   "col_name_font"
    t.integer  "pie_ring_width"
    t.integer  "pie_hole_radius"
    t.string   "text_color"
    t.boolean  "use_shadow",                                 :default => true
    t.string   "shadows"
    t.integer  "value_select_mode",                          :default => 0,     :null => false
    t.string   "select_from"
    t.string   "select_to"
    t.text     "select_choice"
  end

  create_table "profiles", :force => true do |t|
    t.integer  "user_id",                                    :null => false
    t.string   "name"
    t.text     "introduction"
    t.datetime "created_at",                                 :null => false
    t.datetime "updated_at",                                 :null => false
    t.string   "twitter_account"
    t.boolean  "include_twitter_account", :default => false
  end

  create_table "series", :force => true do |t|
    t.integer  "user_id"
    t.string   "name"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "unit"
    t.string   "category_axis_type", :default => "none", :null => false
    t.text     "description"
    t.boolean  "deleted",            :default => false,  :null => false
    t.datetime "deleted_at"
    t.boolean  "is_filter",          :default => false,  :null => false
    t.integer  "filter_target_id"
    t.boolean  "locked",             :default => false
    t.integer  "round_digit",        :default => 8,      :null => false
    t.string   "series_type"
    t.string   "category_axis_unit"
  end

  create_table "series_calculators", :force => true do |t|
    t.integer  "series_collection_id",                :null => false
    t.integer  "sequence",             :default => 0, :null => false
    t.string   "name"
    t.string   "expression"
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
  end

  create_table "series_collections", :force => true do |t|
    t.integer  "user_id"
    t.string   "name",                           :null => false
    t.text     "description"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
    t.boolean  "locked",      :default => false
  end

  create_table "series_filters", :force => true do |t|
    t.string   "filter_type",                       :null => false
    t.text     "description"
    t.integer  "series_id"
    t.string   "reference_value",   :default => "", :null => false
    t.string   "filter_expression", :default => "", :null => false
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
  end

  create_table "series_orders", :force => true do |t|
    t.integer  "series_collection_id"
    t.integer  "series_id"
    t.integer  "sequence",             :default => 0, :null => false
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
    t.string   "alias"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                    :default => ""
    t.string   "encrypted_password",       :default => ""
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",            :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",          :default => 0
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.string   "login"
    t.integer  "series_count",             :default => 0
    t.integer  "series_collections_count", :default => 0
    t.integer  "graphs_count",             :default => 0
  end

  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["login"], :name => "index_users_on_login", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true
  add_index "users", ["unlock_token"], :name => "index_users_on_unlock_token", :unique => true

  create_table "values", :force => true do |t|
    t.integer  "series_id",                                                                                    :null => false
    t.integer  "sequence",                                                                 :default => 0,      :null => false
    t.integer  "value_int",                    :limit => 8
    t.string   "category_axis_label_text"
    t.integer  "category_axis_label_int"
    t.datetime "created_at",                                                                                   :null => false
    t.datetime "updated_at",                                                                                   :null => false
    t.string   "category_axis_type",                                                       :default => "none", :null => false
    t.text     "note"
    t.decimal  "value_float",                               :precision => 24, :scale => 8
    t.datetime "category_axis_label_datetime"
  end

end
