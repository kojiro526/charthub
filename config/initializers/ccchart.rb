CHART_CONFIG = {

  type: "line",
  changeRC: "no",

  width: 600,
  height: 400,
  axisXWidth: 1,
  axisXLen: 10,
  axisYWidth: 1,
  axisYLen: 10,
  onlyChart: "no",
  onlyChartWidthTitle: "no",
  paddingTop: nil,
  paddingBottom: nil,
  paddingLeft: nil,
  paddingRight: nil,
  xColor: "#000",
  yColor: "#000",
  yScaleSkip: 1,
  xScaleRotate: 0,
  yScaleRotate: 0,

  title: "",
  titleColor: "#ddd",
  titleFont:  {size: 28, font: 'san-serif'},
  titleTextAlign: "center",
  titleY: 38,
  subTitle: "",
  subTitleColor: "#ccc",
  subTitleFont: {size: 12, font: 'san-serif'},
  subTitleTextAlign: "center",
  subTitleY: 55,
  xScaleColor: "#aaa",
  xScaleFont: {size: 12, font: 'san-serif'},
  xScaleAlign: "center",
  xScaleXOffset: 0,
  xScaleYOffset: 18,
  xScaleSkip: 1,
  colNamesTitleOffset: 22,
  yScaleColor: "#aaa",
  yScaleFont: {size: 12, font: 'san-serif'},
  yScaleAlign: "right",
  yScaleXOffset: 0,
  yScaleYOffset: 0,
  yScalePercent: "no",
  useHanrei: "yes",
  hanreiColor: "#ccc",
  hanreiFont: {size: 12, font: 'san-serif'},
  hanreiAlign: "left",
  hanreiLineHeight: nil,
  hanreiXOffset: 14,
  hanreiYOffset: 40,
  hanreiRadius: 40,
  hanreiMarkerStyle: "arc",
  unit: "",
  unitColor: "#aaa",
  unitFont: {size: 12, font: 'san-serif'},
  unitAlign: "right",
  unitXOffset: 10,
  unitYOffset: 12,

#  memo: ,
  useVal: "no",
  percentVal: "no",
  useToolTip: "no",
  barTipAnchorColor: "white",
  useMarker: "none",
  useFirstToColName: true,
  useFirstToRowName: true,
  maxY: nil,
  minY: nil,
  roundedUpMaxY: 100,
  maxX: nil,
  minX: nil,
  roundedUpMaxX: 100,
  colorSet: ["red","#FF9114","#3CB000","#00A8A2","#0036C0","#C328FF","#FF34C0"],

  roundDigit: 0,
  barPadding: 1,

  lineWidth: 4,
  borderWidth: nil,
  markerWidth: nil,

  pieDataIndex: nil,
  colNameFont: {size: 28, font: 'san-serif'},
  pieRingWidth: nil,
  pieHoleRadius: nil,
  textColor: nil,

  useShadow: "yes",
#  shadows: {
#    hanrei:  ['#222',5,5,5],
#    xline:   ['#444',7,7,5],
#    line:    ['#222',5,5,5],
#    bar:     ['#222', 5, 5, 5],
#    stacked: ['#222', 5, -5, 5],
#    stackedarea: ['#222', 5, 5, 5],
#    bezi:    ['#222', 5, 5, 5],
#    bezi2:   ['#222', 5, 5, 5],
#    scatter: ['#222', 5, 5, 5],
#    pie:     ['#444', 3, 3, 3]
#   },
  
  barWidth: nil,
  bg: nil,
  bgGradient: {
     direction: "vertical",
     from: "#687478",
     to: "#222222"
   }
}
=begin
width
height
axisXWidth
axisXLen
axisYWidth
axisYLen
onlyChart
onlyChartWidthTitle
paddingTop
paddingBottom
paddingLeft
paddingRight
xColor
yColor
bg
bgGradient
#img
#imgAlpha
title
titleColor
titleFont
titleTextAlign
titleY
subTitle
subTitleColor
subTitleFont
subTitleTextAlign
subTitleY
xScaleColor
xScaleFont
xScaleAlign
xScaleXOffset
xScaleYOffset
xScaleSkip
colNamesTitleOffset
yScaleColor
yScaleFont
yScaleAlign
yScaleXOffset
yScaleYOffset
yScalePercent
hanreiColor
hanreiFont
hanreiAlign
hanreiLineHeight
hanreiXOffset
hanreiYOffset
hanreiRadius
hanreiMarkerStyle
unit
unitColor
unitFont
unitAlign
unitXOffset
unitYOffset
memo
useVal
percentVal
useToolTip
barTipAnchorColor
useMarker
useFirstToColName
useFirstToRowName
maxY
minY
roundedUpMaxY
maxX
minX
roundedUpMaxX
colorSet
=end

