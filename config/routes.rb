Charthub::Application.routes.draw do

  #resources :values
  #resources :series

  devise_for :users

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action
  scope '/:login/' do
    match 'series' => 'members#series', :as => :login_series
    match 'series/:id' => 'series#show', :as => :login_series_show
    match 'series/:id/load' => 'series#load', :as => :login_series_load
    #match 'series_collections' => 'home#series_collections', :as => :login_series_collections
    match 'tables' => 'members#series_collections', :as => :login_series_collections
    #match 'series_collections/:id' => 'series_collections#show', :as => :login_series_collection_show
    match 'tables/:id' => 'series_collections#show', :as => :login_series_collection_show
    #match 'series_collections/:id/load' => 'series_collections#load', :as => :login_series_collections_load
    match 'tables/:id/load' => 'series_collections#load', :as => :login_series_collections_load
    match 'graphs' => 'members#graphs', :as => :login_graphs
    match 'graphs/:id' => 'graphs#show', :as => :login_graph_show
    match 'graphs/:id/load' => 'graphs#load', :as => :login_graph_load
  end
  match 'series/:id/load' => 'series#load', :as => :series_load
  match 'series/:id/desc' => 'series#get_desc', :as => :series_get_desc, :via => :get
  match 'series/preview' => 'series#preview_values', :as => :series_preview_values, :via => :post
  match 'series/:id/values' => 'series#create_values', :as => :series_create_values, :via => :post
  #match 'series_collections/:id/load' => 'series_collections#load', :as => :series_collections_load
  #match 'series_collections/:id/desc' => 'series_collections#get_desc', :as => :series_collections_get_desc, :via => :get
  match 'tables/:id/load' => 'series_collections#load', :as => :series_collections_load
  match 'tables/:id/desc' => 'series_collections#get_desc', :as => :series_collections_get_desc, :via => :get
  match 'graphs/:id/load' => 'graphs#load', :as => :graph_load
  match 'graphs/config_test_new' => 'graphs#config_test_new', :as => :graph_config_test_new, :via => :put
  match 'graphs/:id/config_test_edit' => 'graphs#config_test_edit', :as => :graph_config_test_edit, :via => :put
  match 'graphs/new_memo' => 'graphs#new_memo', :as => :graph_new_memo
  match 'graphs/:id/image' => 'graphs#image', :as => :graph_image
  match 'profile/edit' => 'profiles#edit_myself'
  match 'profile/update' => 'profiles#update_myself', :via => :put
  match 'series/:series_id/graphs/new' => 'graphs#new_for_series', :as => :series_graphs_new
  #match 'series_collections/:series_collection_id/graphs/new' => 'graphs#new_for_series_collection', :as => :series_collection_graphs_new
  #match 'series_collections/:id/series' => 'series_collections#get_series', :via => :get
  #match 'series_collections/:id/collection' => 'series_collections#get_selected_rows', :as => 'series_collection_collection', :via => :get
  match 'tables/:series_collection_id/graphs/new' => 'graphs#new_for_series_collection', :as => :series_collection_graphs_new
  match 'tables/:id/series' => 'series_collections#get_series', :via => :get
  match 'tables/:id/collection' => 'series_collections#get_selected_rows', :as => 'series_collection_collection', :via => :get
  #match 'series_collections/:id/collection' => 'series_collections#update_collection', :as => 'update_series_collection_collection', :via => :put
  match 'tables/:id/collection' => 'series_collections#update_collection', :as => 'update_series_collection_collection', :via => :put
  match 'config/index' => 'config#index', :as => 'config_index', :via => :get

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end
  resources :series do
    resources :values
  end
  resources :profiles
  resources :series_orders
  resources :series_collections, :path => '/tables'
  resources :series_filters
  resources :graphs

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
  match '/home' => 'home#show', :as => :login_home
  match ':login/' => 'members#show', :as => :login_member_home

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => 'top#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'

end
