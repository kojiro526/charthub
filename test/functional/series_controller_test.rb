# -*- coding: utf-8 -*-
require 'test_helper'

class SeriesControllerTest < ActionController::TestCase
  setup do
    @series = series(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:series)
  end

  test "should get new" do
    get :new
    assert_redirected_to '/users/sign_in'

    sign_in :user, users(:one)
    get :new, {}
    assert_response :success
  end

  test "should create series" do
    post :create, series: { name: @series.name }
    assert_redirected_to '/users/sign_in'

    sign_in :user, users(:one)
    assert_difference('Series.count') do
      post :create, series: { name: @series.name }
    end
    assert_redirected_to login_series_show_path(login: 'kojiro', id: assigns(:series).id)

    # user_idを直接指定して更新は出来ないこと。
    assert_raises(ActiveModel::MassAssignmentSecurity::Error){
      post :create, series: { name: @series.name, user_id: @series.user_id }
    }
  end

  test "should show series" do
    get :show, id: @series
    assert_response :success

    get :show, id: 999
    assert_response :not_found
  end

  test "should get edit" do
    get :edit, id: @series
    assert_redirected_to '/users/sign_in'

    sign_in :user, users(:one)
    get :edit, id: @series
    assert_response :success

    get :edit, id: 999
    assert_response :not_found

    get :edit, id: 3
    assert_response :forbidden
  end

  test "should update series" do
    put :update, id: 1, series: { name: @series.name, user_id: @series.user_id }
    assert_redirected_to '/users/sign_in'

    sign_in :user, users(:one)
    put :update, id: 1, series: { name: @series.name }
    assert_redirected_to login_series_show_path(login: users(:one).login, id: assigns(:series).id)

    put :update, id: 999, series: { name: @series.name }
    assert_response :not_found

    put :update, id: 3, series: { name: @series.name }
    assert_response :forbidden

    put :update, id:1, series: {name: @series.name, category_axis_type: 'date_y' }
    assert assigns(:series).category_axis_type == 'none', "category_axis_typeはパラメータから更新されないこと。（入力：date_y 結果:#{assigns(:series).category_axis_type}）"

    # user_idを直接指定して更新は出来ないこと。
    assert_raises(ActiveModel::MassAssignmentSecurity::Error){
      put :update, id: 1, series: { name: @series.name, user_id: @series.user_id }
    }
  end

  test "should destroy series" do
    delete :destroy, id: @series
    assert_redirected_to '/users/sign_in'

    sign_in :user, User.first
    assert_difference('Series.count', -1) do
      delete :destroy, id: @series
    end
    assert_redirected_to login_home_path
  end

  test "should create values" do
    post :create_values, {id: 1, series: { name: @series.name, user_id: @series.user_id }}
    assert_redirected_to '/users/sign_in'
  end

  test "should get load" do
    xhr :get, :load, {id: 1, format: :json} 
    assert_response :success

    xhr :get, :load, {id: 999, format: :json}
    assert_response :not_found
  end

  test "should get description" do
    get :get_desc, id: 1
    assert_response :success

    get :get_desc, id: 999
    assert_response :not_found

    xhr :get, :get_desc, {id: 1}
    assert_response :success

    xhr :get, :get_desc, {id: 999}
    assert_response :not_found
  end

end
