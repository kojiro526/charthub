require 'test_helper'

class SeriesFiltersControllerTest < ActionController::TestCase
  setup do
    @series_filter = series_filters(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:series_filters)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create series_filter" do
    assert_difference('SeriesFilter.count') do
      post :create, series_filter: { child_series_id: @series_filter.child_series_id, description: @series_filter.description, filter_expression: @series_filter.filter_expression, filter_type: @series_filter.filter_type, name: @series_filter.name, reference_value: @series_filter.reference_value, series_id: @series_filter.series_id, user_id: @series_filter.user_id }
    end

    assert_redirected_to series_filter_path(assigns(:series_filter))
  end

  test "should show series_filter" do
    get :show, id: @series_filter
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @series_filter
    assert_response :success
  end

  test "should update series_filter" do
    put :update, id: @series_filter, series_filter: { child_series_id: @series_filter.child_series_id, description: @series_filter.description, filter_expression: @series_filter.filter_expression, filter_type: @series_filter.filter_type, name: @series_filter.name, reference_value: @series_filter.reference_value, series_id: @series_filter.series_id, user_id: @series_filter.user_id }
    assert_redirected_to series_filter_path(assigns(:series_filter))
  end

  test "should destroy series_filter" do
    assert_difference('SeriesFilter.count', -1) do
      delete :destroy, id: @series_filter
    end

    assert_redirected_to series_filters_path
  end
end
