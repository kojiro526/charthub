require 'test_helper'

class SeriesCollectionsControllerTest < ActionController::TestCase
  setup do
    @series_collection = series_collections(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:series_collections)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create series_collection" do
    assert_difference('SeriesCollection.count') do
      post :create, series_collection: { description: @series_collection.description, name: @series_collection.name, user_id: @series_collection.user_id }
    end

    assert_redirected_to series_collection_path(assigns(:series_collection))
  end

  test "should show series_collection" do
    get :show, id: @series_collection
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @series_collection
    assert_response :success
  end

  test "should update series_collection" do
    put :update, id: @series_collection, series_collection: { description: @series_collection.description, name: @series_collection.name, user_id: @series_collection.user_id }
    assert_redirected_to series_collection_path(assigns(:series_collection))
  end

  test "should destroy series_collection" do
    assert_difference('SeriesCollection.count', -1) do
      delete :destroy, id: @series_collection
    end

    assert_redirected_to series_collections_path
  end
end
