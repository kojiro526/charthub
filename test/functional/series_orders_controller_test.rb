require 'test_helper'

class SeriesOrdersControllerTest < ActionController::TestCase
  setup do
    @series_order = series_orders(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:series_orders)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create series_order" do
    assert_difference('SeriesOrder.count') do
      post :create, series_order: { sequence: @series_order.sequence, series_collection_id: @series_order.series_collection_id, series_id: @series_order.series_id }
    end

    assert_redirected_to series_order_path(assigns(:series_order))
  end

  test "should show series_order" do
    get :show, id: @series_order
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @series_order
    assert_response :success
  end

  test "should update series_order" do
    put :update, id: @series_order, series_order: { sequence: @series_order.sequence, series_collection_id: @series_order.series_collection_id, series_id: @series_order.series_id }
    assert_redirected_to series_order_path(assigns(:series_order))
  end

  test "should destroy series_order" do
    assert_difference('SeriesOrder.count', -1) do
      delete :destroy, id: @series_order
    end

    assert_redirected_to series_orders_path
  end
end
