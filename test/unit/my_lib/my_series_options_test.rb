# -*- coding: utf-8 -*-
require 'test_helper'

class MySeriesOptionsTest < ActiveSupport::TestCase

  test "系列の色を正しく取得する。" do

    #result_color_set = ["red","#FF9114","#3CB000"]
    series_ops = Array.new
    series_ops.push << { 
      source: [ 
        { selected: "true", color: "" },
        { selected: "true", color: "" },
        { selected: "true", color: "" }
      ],
      result: ["red","#FF9114","#3CB000"]
    } 

    series_ops.push << {
      source: [
        { selected: "false", color: "" },
        { selected: "true", color: "" },
        { selected: "true", color: "" }
      ],
      result: ["#FF9114","#3CB000"]
    } 

    series_ops.push << {
      source: [
        { selected: "true", color: "" },
        { selected: "false", color: "" },
        { selected: "true", color: "" }
      ],
      result: ["red","#3CB000"]
    } 

    series_ops.push << { 
      source: [
        { selected: "true", color: "" },
        { selected: "true", color: "" },
        { selected: "false", color: "" }
      ],
      result: ["red","#FF9114"]
    } 

    series_ops.push << { 
      source: [
        { selected: "true", color: "#FFF" },
        { selected: "true", color: "#FF0" },
        { selected: "true", color: "#F00" }
      ],
      result: ["#FFF","#FF0","#F00"]
    } 

    series_ops.push << { 
      source: [
        { selected: "false", color: "#FFF" },
        { selected: "true", color: "#FF0" },
        { selected: "true", color: "#F00" }
      ],
      result: ["#FF0","#F00"]
    } 

    series_ops.push << { 
      source: [
        { selected: "true", color: "#FFF" },
        { selected: "false", color: "#FF0" },
        { selected: "true", color: "#F00" }
      ],
      result: ["#FFF","#F00"]
    } 

    series_ops.push << { 
      source: [
        { selected: "true", color: "#FFF" },
        { selected: "true", color: "#FF0" },
        { selected: "false", color: "#F00" }
      ],
      result: ["#FFF","#FF0"]
    } 

    series_ops.push << { 
      source: [
        { selected: "false", color: "#FFF" },
        { selected: "true", color: "#FF0" },
        { selected: "false", color: "#F00" }
      ],
      result: ["#FF0"]
    } 

    series_ops.each_with_index{|op, i|
      ops = MyLib::MySeriesOptions.create_by_params(op[:source])
      conf = ops.to_config
      assert conf[:colorSet] == op[:result] , "配色取得エラー（No.#{i}）。 期待：#{op[:result].join(',')} 結果：#{conf[:colorSet].join(',')} "
    }

  end

end
