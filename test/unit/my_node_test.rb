# -*- coding: utf-8 -*-
require 'test_helper'

class MyNodeTest < ActiveSupport::TestCase

  test "eval" do
    p = MyLib::MyEval::MyParser.new

    src = [
      ["1+2", 3],
      ["1+2-3", 0],
      ["1+2-4", -1],
      ["1+(2-3)", 0],
      ["1+3*(2-3)", -2],
      ["1+$1", 2],
      ["$1+$2*$3", 1001],
      ["2^3", 8]
    ]

    src.each{|exp|
      nodes = p.parse(exp[0])
      table = MyLib::MyEval::MyVariableTable.new
      table.set_value("$1", 1)
      table.set_value("$2", 10)
      table.set_value("$3", 100)
      nodes.set_variable_table(table)
      res = nodes.eval
      assert res == exp[1], "evalエラー。式：#{exp[0]} 評価結果：#{res} 期待：#{exp[1]}"
    }
  end
end

