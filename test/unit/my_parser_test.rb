# -*- coding: utf-8 -*-
require 'test_helper'

class MyParserTest < ActiveSupport::TestCase

  test "get operator position" do

    parser = MyLib::MyEval::MyParser.new
    src = [
      ["1+2-3", 3],
      ["1*2-3", 3],
      ["1+(2-3)", 1],
      ["1+2^3+4", 5]
    ]
    src.each{|exp|
      pos = parser.get_operator_pos(exp[0])
      assert pos == exp[1], "低優先度演算子位置エラー。式：#{exp[0]} 結果：#{pos}(exp[0][pos])"
    }

  end

  test "remove bracket" do

    parser = MyLib::MyEval::MyParser.new
    src = [
      ["(1+2)", "1+2"],
      ["((1*2))", "1*2"],
      ["(((1*2)))", "1*2"]
    ]
    src.each{|exp|
      exp_tmp = parser.remove_bracket(exp[0])
      assert exp_tmp == exp[1], "括弧削除エラー。式：#{exp[0]} 結果：#{exp_tmp} 期待：#{exp[1]}"
    }


  end

  test "parse" do

    parser = MyLib::MyEval::MyParser.new

    src = [
      ["1+2", ["1","+","2"], ["1","2","+"]],
      ["1+2-3", ["1","+","2","-","3"], ["1","2","+","3","-"]],
      ["11+12-13", ["11","+","12","-","13"],["11","12","+","13","-"]],
      ["1.1+2.2-3.3", ["1.1","+","2.2","-","3.3"],["1.1","2.2","+","3.3","-"]],
      ["1+(2-3)", ["1","+","(","2","-","3",")"],["1","2","3","-","+"]],
      ["123+$123", ["123","+","$123"],["123","$123","+"]]
    ]
    src.each{|exp|
      res = parser.parse(exp[0])
      res2 = parser.parse(exp[0]).traverse_postorder
      assert res2 == exp[2], "トラバースエラー。式：#{exp[0]} トラバース結果：'#{res2.join(',')}' 期待：'#{exp[2].join(',')}'"
    }

  end

end
