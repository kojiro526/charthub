# -*- coding: utf-8 -*-
require 'test_helper'

class SeriesTest < ActiveSupport::TestCase
  test "項目軸のタイプは決められた文字列に限定される" do
     s = series(:one)
     s.category_axis_type = 'test'
     assert !s.valid?, "category_axis_typeに許可されていない文字列が設定された場合はエラーとなるべき。(#{s.category_axis_type})"
  end

  test "テキストから値を生成する" do
    s = series(:one)
text = <<"EOF"
aaa,100,desc1
bbb,200,desc2
ccc,300,desc3
EOF
    vs = s.build_values_by_text(text)
    assert vs.size == 3, "3件の値が生成されること。（#{vs.size}）"
    vs.each{|v|
      assert v.class.name == 'Value', "Valueオブジェクトの配列が返されること。（#{v.class.name}）"
    }
    assert vs[0].label == 'aaa'
    assert vs[1].label == 'bbb'
    assert vs[2].label == 'ccc'
    assert vs[0].value == 100
    assert vs[1].value == 200
    assert vs[2].value == 300
  end
end
