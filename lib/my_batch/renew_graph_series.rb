class MyBatch::RenewGraphSeries
  def self.renew

    logger = Rails.logger

    logger.info("START renew_graph_series.rb")
    logger.info("#{Time.now}")

    ActiveRecord::Base.transaction do

#    graphs_all = Graph.all
#    graphs = [graphs_all.first]
#    graphs_tmp = Graph.find(90)
#    graphs = [graphs_tmp]
    graphs = Graph.all
    graphs.each{|graph|

      logger.info("graph.id=#{graph.id}")

      next if graph.graphable_type == 'SeriesCollection' && graph.series_option.blank?

      graph.graph_series.each{|gs|
        gs.destroy
      }

      case graph.graphable_type
      when 'Series'
        logger.info("graphable_type=#{graph.graphable_type}")
        series = Series.find(graph.graphable_id)
        gs = series.graph_series.build({sequence: 0, selected: true, series_type: "Series" })
        logger.info(gs)
        graph.graph_series << gs
      when 'SeriesCollection'
        logger.info("graphable_type=#{graph.graphable_type}")
        ops = JSON.parse(graph.series_option)
        i=0
        ops.each{|op|

          next if op["series_type"].blank?

          case op["series_type"]
            when "Series","SeriesOrder"
              series = Series.find(op["id"])
            when "SeriesCalculator"
              series = SeriesCalculator.find(op["id"])
            else
              #raise "Error: series_type=#{p[:series_type]}"
          end

          gs = series.graph_series.build({sequence: i, selected: op["selected"], series_type: op["series_type"] })
          graph.graph_series << gs
          logger.info(gs)
          i+=1
        }
      end
    }

#    raise 'TEST'

    end

    logger.info("#{Time.now}")
    logger.info("END renew_graph_series.rb")

  end
end
