class MyLib::MySeriesOptions < Array

  def initialize
    #@key_lcc = [:colorSet, :useVal, :valFont, :valColor, :valYOffset, :valXOffset]
    #@key_uscore = [:color, :use_val, :val_font, :val_color, :val_y_offset, :val_x_offset]
  end

  # ccchartのコンフィグ形式に変換する
  def to_config

    graph_config = Hash.new

    # 系列設定の初期化
    # colorSetはデフォルト値で全系列分初期化
    graph_config[:colorSet] = Array.new
    c = 0
    self.each{|op|
      if op.selected == true
        graph_config[:colorSet].push CHART_CONFIG[:colorSet][c]
      end

      c += 1
      if c == CHART_CONFIG[:colorSet].size
        c = 0 
      end
    }

    graph_config[:useVal] = Array.new unless self.use_val_is_all_blank?
    graph_config[:valFont] = Array.new unless self.val_font_is_all_blank?
    graph_config[:valColor] = Array.new unless self.val_color_is_all_blank?
    graph_config[:valYOffset] = Array.new unless self.val_y_offset_is_all_blank?
    graph_config[:valXOffset] = Array.new unless self.val_x_offset_is_all_blank?
    i=0
    self.each{|op|
      next unless op.selected == true

      graph_config[:colorSet][i] = op.color unless op.color.blank?

      unless graph_config[:useVal].nil?
        if op.use_val == true
          graph_config[:useVal].push "yes"
        else
          graph_config[:useVal].push "no"
        end
      end

      unless graph_config[:valFont].nil? 
        if !op.val_font.blank? && !op.val_font[:size].blank? && !op.val_font[:font].blank?
          graph_config[:valFont].push "100 #{op.val_font[:size]}px #{op.val_font[:font]}"
        else
          graph_config[:valFont].push ""
        end
      end
      graph_config[:valColor].push(op.val_color) if !graph_config[:valColor].nil?
      graph_config[:valYOffset].push(op.val_y_offset) if !graph_config[:valYOffset].nil?
      graph_config[:valXOffset].push(op.val_x_offset) if !graph_config[:valXOffset].nil?
      i += 1
    }

    return graph_config

  end

  def selected_is_changed?
    self.each{|op|
      return true if op.selected_is_changed?
    }
    return false
  end

  # パラメータから生成する。
  def self.create_by_params(params)
    series_options = MyLib::MySeriesOptions.new
    params.each{|op|
      gs = MyLib::MySeriesOption.new

      gs.selected_reg = ( op[:selected_reg] == "true" )
      gs.selected = ( op[:selected] == "true" ) # チェックされていない場合はnil

      gs.color = op[:color] unless op[:color].blank?

      if !op[:use_val].blank?
        op[:use_val] == "true" ? gs.use_val = true : gs.use_val = false
      end

      gs.val_color = op[:val_color] unless op[:val_color].blank?

      if !op[:val_font].blank? && !op[:val_font][:size].blank? && !op[:val_font][:font].blank?
        gs.val_font = {size: op[:val_font][:size].to_i, font: op[:val_font][:font]}
      else
        gs.val_font = nil
      end

      gs.val_y_offset = op[:val_y_offset].to_i unless op[:val_y_offset].blank?
      gs.val_x_offset = op[:val_x_offset].to_i unless op[:val_x_offset].blank?

      gs.series_type = op[:series_type] unless op[:series_type].blank?
      gs.series_id = op[:id].to_i unless op[:id].blank?

      series_options.push(gs)
    }
    return series_options
  end

  # すべての設定がblankかを判定する。
  def color_is_all_blank?
    self.each{|op|
      return false if !op.color.blank?
    }
    return true
  end

  def use_val_is_all_blank?
    self.each{|op|
      return false if !op.use_val.blank?
    }
    return true
  end

  def val_y_offset_is_all_blank?
    self.each{|op|
      return false if !op.val_y_offset.blank?
    }
    return true
  end

  def val_x_offset_is_all_blank?
    self.each{|op|
      return false if !op.val_x_offset.blank?
    }
    return true
  end

  def val_color_is_all_blank?
    self.each{|op|
      return false if !op.val_color.blank?
    }
    return true
  end

  def val_font_is_all_blank?
    self.each{|op|
      return false if !op.val_font.blank?
    }
    return true
  end

end
