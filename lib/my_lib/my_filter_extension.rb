module MyLib::MyFilterExtension

  # 通常の系列であれば紐付くvaluesを返すところ、
  # 計算された結果を返すようにメソッドを上書きする。
  def values(*args)


    if args[0] == true
      @series_origin = Series.find(filter_target_id)
      @values_cache = Array.new
      filtering_values
    else
      if @values_cache.nil?
        @values_cache = Array.new
        filtering_values
      end
    end

    return @values_cache
    
  end

  # filterの場合は項目ラベルの種別はオリジナルの系列から返すように修正。
  def category_axis_type
    @series_origin = Series.find(filter_target_id)
    return @series_origin.category_axis_type
  end

protected

  # 参照する値が設定されている場合、その値を取得する。
  def get_reference_value
    ref_value = nil
    @series_origin.values(true).each{|value|
      if value.label == self.series_filter.reference_value
        ref_value = value.value
      end
    }
    return ref_value
  end

  # フィルタを適用する。
  def filtering_values

    case self.series_filter.filter_type
      when 'relativezation'
        @series_origin.values(true).each{|value|
          value_tmp = MyLib::MyValue.new
          value_tmp.category_axis_type = value.category_axis_type
          value_tmp.label = value.label
          value_tmp.value = value_filter(value.value)
          value_tmp.category_axis_label = value.category_axis_label
          @values_cache.push(value_tmp)
        }
      when 'amount_of_change'
        @series_origin.values(true).each_with_index{|value, i|
          value_tmp = MyLib::MyValue.new
          value_tmp.label = value.label
          value_tmp.category_axis_type = value.category_axis_type
          if i > 0
            prev = @series_origin.values[i-1]
            value_tmp.value = value.value - prev.value if !value.value.blank? && !prev.value.blank?
          end
          @values_cache.push(value_tmp)
        }
      when 'rate_of_change'
        @series_origin.values(true).each_with_index{|value, i|
          value_tmp = MyLib::MyValue.new
          value_tmp.label = value.label
          value_tmp.category_axis_type = value.category_axis_type
          if i > 0
            prev = @series_origin.values[i-1]
            value_tmp.value = (value.value.to_f / prev.value.to_f - 1) * 100 if !value.value.blank? && !prev.value.blank?
          end
          @values_cache.push(value_tmp)
        }
      else
    end


  end

  def value_filter(value)
    if self.series_filter.filter_type == 'relativezation'
      return value.to_f / @ref_value.to_f
    else
      return 0
    end
  end


end
