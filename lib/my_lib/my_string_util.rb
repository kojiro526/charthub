module MyLib::MyStringUtil

  def to_lcc

    self.split('_').map.with_index{|str, index|
      if index == 0
        str.downcase
      else
        str.capitalize
      end
    }.join

  end

end
