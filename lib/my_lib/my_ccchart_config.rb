class MyLib::MyCcchartConfig < Hash

  def self.default

    graph_default = MyLib::MyCcchartConfig.new

    CHART_CONFIG.each{|key, val|
      case key
        when :type
          graph_default["chart_type"] = val
        when :changeRC
          # Boolean型はyes,noでデフォルト値に保存されているので、1,0に変換
          graph_default["change_r_c"] = self.convStrToBool(val)
        when :useVal, :percentVal, :useToolTip, :yScalePercent, :useHanrei
          # Boolean型はyes,noでデフォルト値に保存されているので、1,0に変換
          graph_default[key.to_s.underscore] = self.convStrToBool(val)
        else 
          graph_default[key.to_s.underscore] = val
      end
    }

    return graph_default
  
  end

  def self.default_to_chart

    graph_config = Hash.new
    CHART_CONFIG.each{|key, val|
      # nil値は自動計算する項目のためハッシュに含めない。
      unless val.nil?
        case key
          when :titleFont, :subTitleFont, :xScaleFont, :yScaleFont, :hanreiFont, :unitFont
            # font系の処理
            graph_config[key] = "100 #{val[:size]}px #{val[:font]}"
          else
            graph_config[key] = val
        end
      end
    }
    return graph_config

  end

  def self.convStrToBool(val)
    if val == "yes"
      true
    else
      false
    end
  end

  def self.convBoolToStr(val)
    if val == true
      "yes"
    else
      "no"
    end
  end

end
