class MyLib::MyTemporaryTableCell

  attr_accessor :col_properties

  def initialize
    @value = nil
    @options = Hash.new
    @col_properties = Hash.new
  end

  def value_is_nil?
    return @value.nil?
  end

  def value=(val)
    @value = val
  end

  def value

    return nil if @value.nil?

    if @col_properties[:round_digit]
      return  @value.round(@col_properties[:round_digit])
    else
      return @value
    end

  end

end
