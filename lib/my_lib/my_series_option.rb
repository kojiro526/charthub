class MyLib::MySeriesOption

  attr_accessor :series_id, :series_class, :series_type, :selected, :selected_reg, :name,
  :color, :color_template, :use_val, :val_color, :val_y_offset, :val_x_offset, :val_font

  def initialize
    @series_id = nil
    @series_class = '' # Series|SeriesCalculator
    @series_type = '' # Series|SeriesOrder|SeriesCalculator
    @selected = false
    @selected_reg = false # グラフのテスト描画時に、選択状態が変更したかを比較するための一時保存
    @name = ''
    @color = ''
    @color_template = ''
    @use_val = nil
    @val_color = ''
    @val_font = {size: 12, font: 'san-serif'}
    @val_y_offset = nil
    @val_x_offset = nil
  end

  def get_color
    self.color.blank? ? self.color_template : self.color
  end

  def selected_is_changed?
    return selected != selected_reg
  end

end
