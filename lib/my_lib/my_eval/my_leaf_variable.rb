class MyLib::MyEval::MyLeafVariable < MyLib::MyEval::MyLeaf

  def eval

    raise "No variable table." if @variable_table.nil?

    return @variable_table.get_value(@expression)
    
  end

end

