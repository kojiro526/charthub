class MyLib::MyEval::MyVariableTable

  attr_accessor :table

  def initialize
    @table = Hash.new
  end

  def get_value(exp)
    return @table[exp]
  end

  def set_value(var, num)
     @table[var] = num
  end

end
