class MyLib::MyEval::MyLeaf < MyLib::MyEval::MyNode

  attr_accessor :expression, :node_type

  def initialize
    @expression = ""
    @node_type = :leaf
  end

  def traverse_postorder
    return [@expression]
  end

  def eval
  end

  def self.create(exp)
    res = nil

    #数字型
    if exp.match(/^[+-]*\d[\d\.]*$/)
       res = MyLib::MyEval::MyLeafNumber.new
       res.expression = exp
       return res
    end

    #変数型
    if exp.match(/^\$\d+$/)
       res = MyLib::MyEval::MyLeafVariable.new
       res.expression = exp
       return res
    end

    #関数型
  end

end
