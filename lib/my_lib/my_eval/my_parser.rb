class MyLib::MyEval::MyParser

  def parse(str)

    op_pos = self.get_operator_pos(str)

    if op_pos.nil? || op_pos == 0 || op_pos == str.size-1
      #node = MyLib::MyEval::MyLeaf.new
      node = MyLib::MyEval::MyLeaf.create(str)
      #node.expression = str 
    else
      node = MyLib::MyEval::MyNode.new
      node.expression = str
      node.operator = str[op_pos]
      node.left_node = self.parse(remove_bracket(str[0..(op_pos-1)]))
      node.right_node = self.parse(remove_bracket(str[(op_pos+1)..-1]))
    end

    return node

  end

  # ()で囲まれている場合、削除する。
  def remove_bracket(str)
    m = str.match(/^\((.*)\)$/)
    if m.nil?
      return str
    else
      return self.remove_bracket(m[1])
    end
  end

  #最も優先度の低い演算子の場所を返す。無い場合はnil
  def get_operator_pos(str)
    pos = nil
    nest = 0
    priority = 5
    priority_lowest = 4

    str.chars.each_with_index{|c, i|
      case c
      when /\+|\-/
        priority = 1
      when /\*|\//
        priority = 2
      when "^"
        priority = 3
      when "("
        nest += 1
      when ")"
        nest -= 1
      end

      #プライオリティがレジスタの値より低ければそれをレジスタに代入
      #（）で囲まれている場合は処理の対象外
      if nest == 0 && !priority.nil? && priority <= priority_lowest
        priority_lowest = priority 
        pos = i
      end

      priority = nil
    }

    return pos

  end

end
