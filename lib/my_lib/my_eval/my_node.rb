class MyLib::MyEval::MyNode

  attr_accessor :expression, :operator, :left_node, :right_node, :node_type, :variable_table

  def initialize
    @expression = ""
    @operator = ""
    @left_node = nil
    @right_node = nil
    @node_type = :node
    @variable_table = nil #変数テーブル
  end

  def traverse_postorder

    arr = Array.new
    if !self.left_node.nil?
      arr.concat(self.left_node.traverse_postorder)
    end

    if !self.right_node.nil?
      arr.concat(self.right_node.traverse_postorder)
    end

    arr << self.operator

    return arr

  end

  def eval

    res = nil

    l_res = @left_node.eval
    r_res = @right_node.eval

    return nil if l_res.nil? || r_res.nil?

    case @operator
    when '+'
      res = @left_node.eval + @right_node.eval
    when '-'
      res = @left_node.eval - @right_node.eval
    when '*'
      res = @left_node.eval * @right_node.eval
    when '/'
      res = @left_node.eval.to_f / @right_node.eval.to_f
    when '^'
      res = @left_node.eval ** @right_node.eval
    end

    return res
  end

  def set_variable_table(table)
    @variable_table = table
    if @node_type == :node
      @left_node.set_variable_table(table) unless @left_node.nil?
      @right_node.set_variable_table(table) unless @right_node.nil?
    end
  end

end
