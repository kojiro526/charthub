class MyLib::MyEval::MyLeafNumber < MyLib::MyEval::MyLeaf

  def eval
    @expression.to_f
  end

end
