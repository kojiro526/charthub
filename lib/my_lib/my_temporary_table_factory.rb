# -*- coding: utf-8 -*-
class MyLib::MyTemporaryTableFactory

  attr_accessor :series_collection, :series_orders, :series_calculators, :rows, :view_options

  def initialize
    #@series_collection = nil
    #@series_orders = nil
    @series_collection = nil
    @series_orders = Array.new
    @series_calculators = Array.new
    #@rows = Hash.new # MyTemporaryTableRowの集合
    @view_options = nil
  end

  # 実系列と計算系列から行を生成する。
  def eval

    # 列ごとのプロパティを取得
    col_prop = self.col_properties  

    # 列数を取得
    max_cols = @series_orders.size + @series_calculators.size

    # 系列集の実系列ごとに処理
    rows = Hash.new
    @series_orders.each{|order|
      # 一つの系列ごとに全値をセットする。
      # ハッシュのキーは項目ラベル（2011,2012..）とする。
      order.series_with_deleted.values.each{|value|
        if rows[value.label].blank?
          rows[value.label] = MyLib::MyTemporaryTableRow.new
          # セルの数を初期化（しないと8あるべきところ7になったりする）
          # するとTABLEに出力したときに右側の列で足りないところが歯抜けになったりする。
          rows[value.label].col_properties = col_prop # 列ごとのプロパティを行毎に参照できるようにする。
          # UPD 2014-09-09 以下のinitialize_cells内でプロパティを各セルにセットするため、
          #                上行のプロパティセットと順序を入れ替え。
          rows[value.label].initialize_cells(max_cols) 
          rows[value.label].label = value.label
        end
        rows[value.label].set_value(order.sequence, value.value)
      }
    }

    # 行毎に計算系列を評価。
    rows.each{|key, row|
      row.series_calculators = @series_calculators
      row.calculate
    } 

    #項目ラベルをキーにソートする。
    #UPD 2014-10-07 項目ラベルのタイプに応じてソートするかしないかを決定。
    if @series_orders.size > 0 && @series_orders[0].series_with_deleted.category_axis_is_sortable?
      # ここでソートした段階で@rowはHashからArrayになる。
      rows = rows.sort{|(k1, v1), (k2, v2)|
        k1<=>k2
      }
    else
      # 明示的にHashをArrayに変換
      rows = rows.to_a
    end

    my_temporary_table = MyLib::MyTemporaryTable.new
    my_temporary_table.rows = rows
    my_temporary_table.series_collection = @series_collection
    my_temporary_table.series_orders = @series_orders
    my_temporary_table.series_calculators = @series_calculators
    my_temporary_table.col_properties = col_prop

    return my_temporary_table

  end

=begin
  def header

    header = Array.new

    # 実系列のヘッダー情報を収集する。
    @series_orders.each{|order|
      header << {
        :series_type => 'substantial',
        :seq => order.sequence,
        :name => order.series.name,
        :alias => order.alias,
        :source_id => order.series_id
      }
    }

    # 計算系列のヘッダー情報を収集する。
    @series_calculators.each{|culc|
      header << {
        :series_type => 'calcuration',
        :seq => culc.sequence,
        :name => culc.name
      }
    }

    header.sort!{|a, b|
      a[:seq]<=>b[:seq]
    }

    return header

  end
=end

  # 列ごとのプロパティを取得する。
  def col_properties

    properties = Array.new

    # 実系列のヘッダー情報を収集する。
    @series_orders.each{|order|
      properties << {
        :source => order,
        :series_type => 'substantial',
        :seq => order.sequence,
        :name => order.series_with_deleted.name,
        :alias => order.alias,
        :source_id => order.series_id,
        :round_digit => order.series_with_deleted.round_digit,
        :view_option => nil
      }
    }

    # 計算系列のヘッダー情報を収集する。
    @series_calculators.each{|culc|
      properties << {
        :source => culc,
        :series_type => 'calcuration',
        :seq => culc.sequence,
        :name => culc.name,
        :alias => nil,
        :source_id => nil,
        :round_digit => nil,
        :view_option => nil
      }
    }

    properties.sort!{|a, b|
      a[:seq]<=>b[:seq]
    }

    unless view_options.nil?
      properties.each_with_index{|p,i|
        p[:view_option] = view_options[i]
      }
    end

    return properties

  end

=begin
  def to_json_for_chart

    series_table_tmp = Array.new
    series_table_tmp[0] = Array.new # 項目ラベル用配列
    series_table_tmp[0].push('年度') # TODO: 設定値を入れるよう修正すること。

    # 系列のタイトルを設定。
    #self.header.each.with_index(1){|head, i|
    self.col_properties.each.with_index(1){|head, i|
      series_table_tmp[i] = Array.new
      # 系列の名前として、エイリアスがあればエイリアスを設定する。
      if head[:alias].blank?
        series_table_tmp[i].push(head[:name])
      else
        series_table_tmp[i].push(head[:alias])
      end
    }

    @rows.each{|key, row|
      # 項目ラベルを設定
      series_table_tmp[0].push(key)
      # 各系列の値を設定
      row.cells.each.with_index(1){|cell, i|
        if cell.nil?
          series_table_tmp[i].push(nil)
        else
          series_table_tmp[i].push(cell.value)
        end
      }
    }

    return series_table_tmp

  end

  # 設定を元に表示する系列を判定。
  # 引数となる設定は以下のフォーマット
  # [{
  #   series_type: 系列の種別を判定するためのクラス名
  #   id: 実系列の場合はseries_orderのIDではなく実系列のID, 単独系列か計算系列の場合はそれぞれのID
  #   selected: グラフの表示の有無
  # }]
  def to_json_for_chart_with_option(op)
    # 全ての系列を配列で取得
    json = self.to_json_for_chart

    view_list = Array.new
    view_list[0] = true # 項目ラベルは必ず表示

    # 系列コレクションに紐付く全ての実系列、計算系列について、オプションと照合して値を設定。
    srcs = self.series_collection.sources
    if op.size == 0
      # オプションンが全くない場合、何らかの理由で初期状態と判断して全て表示する。
      srcs.each{|series|
        view_list[series.sequence+1] = true
      }
    else
      srcs.each{|series|
        view_list[series.sequence+1] = false
        op.each{|o|
          case series.class.name
          when "SeriesOrder"
            # 実系列の場合はseries_orderのIDではなく実系列（series）のIDと照合する。
            #if series.class.name == o['series_type'] && series.series_id == o['id'] && o['selected'] == true
            if series.class.name == o.series_type && series.series_id == o.series_id && o.selected == true
              view_list[series.sequence+1] = true
            end
          when "Series","SeriesCalculator"
            #if series.class.name == o['series_type'] && series.id == o['id'] && o['selected'] == true
            if series.class.name == o.series_type && series.id == o.series_id && o.selected == true
              view_list[series.sequence+1] = true
            end
          end
        }
      }
    end

    arr = Array.new
    json.each_with_index{|j, i|
      if view_list[i] == true
        arr << j
      end
    } 
    return arr
  end

  def category_axis
    labels = MyLib::MyCategoryAxisLabels.new
    @rows.each{|key, row|
      label = MyLib::MyCategoryAxisLabel.new
      label.text = row.label
      labels.push(label)
    }
    axis = MyLib::MyCategoryAxis.new
    axis.labels = labels
    return axis
  end
=end
end
