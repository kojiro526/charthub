class MyLib::MyYearMonth

  attr_reader :value

  def initialize(*arr)

    var = arr.size > 0 ? arr[0] : nil

    @value = 0

    unless var.nil?
      if var.kind_of?(String)
        @value = str_to_num(var)
      elsif var.kind_of?(Fixnum)
        @value = var
      elsif var.kind_of?(Float)
        @value = var.to_i
      else
        raise 'Input variable is invalid.' + ": #{var.class}"
      end
    end

  end

  # TODO: yyyy-mmを前提(ISO8601)にしているが、他のフォーマットに対応するか検討。
  def str_to_num(var)

    lo_year,lo_month = var.split("-") 
    year_tmp = lo_year.to_i
    month_tmp = lo_month.to_i

    return (year_tmp - 1) * 12 + month_tmp

  end

  def to_s

    return sprintf("%d",year) + "-" + sprintf("%d", month)

  end

  def year
    #year = ((@value - 1) / 12) + 1
    return ((@value - 1) / 12) + 1
  end

  def year=(var)

    int = 0

    if var.kind_of?(String)
      int = var.to_i
    elsif var.kind_of?(Numeric)
      int = var
    else
      raise "Input value is invalid.(#{var.class})"
    end

    month_tmp = self.month
    @value = set_year_month(int, month_tmp)

  end

  def month
    #month = @value - ((year - 1) * 12)
    return @value - ((year - 1) * 12)
  end

  # TODO: 12以上の値を代入した時には年に繰り上げするように実装する。
  def month=(var)
  end

  def set_year_month(im_year, im_month)
    @value = (im_year -1) * 12 + im_month
  end

  def to_i
    return @value
  end

end
