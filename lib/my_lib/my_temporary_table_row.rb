class MyLib::MyTemporaryTableRow

  attr_accessor :label, :cells, :series_calculators, :col_properties

  def initialize
    @label = nil
    @cells = Array.new
    @series_calculators = nil
    @col_properties = nil
  end

  def cells
    return @cells
  end

  # 列の数で初期化する。
  # 実系列と計算系列で8列あれば0-7まで。
  # ADD 2014-09-09
  # 初期鍛冶にすべてCellオブジェクトで初期化するよう修正。
  # nilだとcol_propertiesが設定できず、view_optionが無いため常に表示されてしまう。
  def initialize_cells(num)
    #@cells[num-1] = nil
    num.times{|i|
      @cells[i] = MyLib::MyTemporaryTableCell.new
      @cells[i].col_properties = @col_properties[i]
    }
  end

  def set_value(seq, val)
    #@cells[seq] = val
#    @cells[seq] = MyLib::MyTemporaryTableCell.new
    @cells[seq].value = val unless @cells[seq].blank?
#    @cells[seq].col_properties = @col_properties[seq] # 列ごとのプロパティをセル内でも参照できるようにする。
    #@cells[seq].col_properties = @col_properties[seq-1] #TODO -1の妥当性検証  列ごとのプロパティをセル内でも参照できるようにする。
  end

  # 計算系列を評価する。
  def calculate
    # 該当行の変数テーブルを取得
    var_table = self.create_variable_table
    # 計算系列の評価順序を決定する。
    calculators = self.sort_calculator

    # 各計算系列を評価する。
    calculators.each{|calc|
      val = calc.eval(var_table)
      self.set_value(calc.sequence, val)
      # 評価した計算系列の値は変数テーブルに追加する。(後続の計算系列に利用される。）
      # 変数名はsequence+1が正しいため修正。
      var_table.set_value("$#{calc.sequence+1}", val) 
    }
  end

  # 計算系列の式の依存関係をチェックして順序を決める。
  # TODO: 後で実装する。
  def sort_calculator
    return @series_calculators
  end

  # 変数を置換するための変数テーブルを作成
  # TODO: 変数は順番によって決定される仕様でいいのか？($1,$2...)
  def create_variable_table

    var_table = MyLib::MyEval::MyVariableTable.new

    @cells.each.with_index(1){|v, i|
      #var_table.set_value("$#{i}", v) unless v.nil?
      var_table.set_value("$#{i}", v.value) unless v.nil?
    }

    return var_table

  end

end
