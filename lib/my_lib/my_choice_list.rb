class MyLib::MyChoiceList < Array
  def selected?(str)
    return self.include?(str)
  end
end
