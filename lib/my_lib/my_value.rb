class MyLib::MyValue

  attr_accessor :value, :label, :category_axis_label, :note, :category_axis_type

  def initialize
    @value = nil
    @label = ''
    @category_axis_label = ''
    @category_axis_type  = ''
    @note = ''
  end

end
