class MyLib::MyTemporaryTable

  attr_accessor :series_collection, :series_orders, :series_calculators, :rows, :view_options, :col_properties

  def initialize
    @series_collection = nil
    @series_orders = Array.new
    @series_calculators = Array.new
    @rows = Array.new # MyTemporaryTableRowの集合
    @view_options = nil
    @col_properties = nil
  end

  def to_json_for_chart

    series_table_tmp = Array.new
    series_table_tmp[0] = Array.new # 項目ラベル用配列
    series_table_tmp[0].push('年度') # TODO: 設定値を入れるよう修正すること。

    # 系列のタイトルを設定。
    #self.header.each.with_index(1){|head, i|
    self.col_properties.each.with_index(1){|head, i|
      series_table_tmp[i] = Array.new
      # 系列の名前として、エイリアスがあればエイリアスを設定する。
      if head[:alias].blank?
        series_table_tmp[i].push(head[:name])
      else
        series_table_tmp[i].push(head[:alias])
      end
    }

    @rows.each{|key, row|
      # 項目ラベルを設定
      series_table_tmp[0].push(key)
      # 各系列の値を設定
      row.cells.each.with_index(1){|cell, i|
        if cell.nil?
          series_table_tmp[i].push(nil)
        else
          series_table_tmp[i].push(cell.value)
        end
      }
    }

    return series_table_tmp

  end

  # 設定を元に表示する系列を判定。
  # 引数となる設定は以下のフォーマット
  # [{
  #   series_type: 系列の種別を判定するためのクラス名
  #   id: 実系列の場合はseries_orderのIDではなく実系列のID, 単独系列か計算系列の場合はそれぞれのID
  #   selected: グラフの表示の有無
  # }]
  def to_json_for_chart_with_option(op)
    # 全ての系列を配列で取得
    json = self.to_json_for_chart

    view_list = Array.new
    view_list[0] = true # 項目ラベルは必ず表示

    # 系列コレクションに紐付く全ての実系列、計算系列について、オプションと照合して値を設定。
    srcs = self.series_collection.sources
    if op.size == 0
      # オプションが全くない場合、何らかの理由で初期状態と判断して全て表示する。
      srcs.each{|series|
        view_list[series.sequence+1] = true
      }
    else
      srcs.each{|series|
        view_list[series.sequence+1] = false
        op.each{|o|
          case series.class.name
          when "SeriesOrder"
            # 実系列の場合はseries_orderのIDではなく実系列（series）のIDと照合する。
            #if series.class.name == o['series_type'] && series.series_id == o['id'] && o['selected'] == true
            if series.class.name == o.series_type && series.series_id == o.series_id && o.selected == true
              view_list[series.sequence+1] = true
            end
          when "Series","SeriesCalculator"
            #if series.class.name == o['series_type'] && series.id == o['id'] && o['selected'] == true
            if series.class.name == o.series_type && series.id == o.series_id && o.selected == true
              view_list[series.sequence+1] = true
            end
          end
        }
      }
    end

    arr = Array.new
    json.each_with_index{|j, i|
      if view_list[i] == true
        arr << j
      end
    } 
    return arr
  end

  # 行を範囲選択した結果を返す。
  def range(from, to)
    tmp_table = MyLib::MyTemporaryTable.new
    tmp_table.series_collection = self.series_collection
    tmp_table.series_orders = self.series_orders
    tmp_table.series_calculators = self.series_calculators
    tmp_table.col_properties = self.col_properties
    tmp_table.view_options = self.view_options
    fg = false
    self.rows.each{|row|
      fg = true if row[1].label == from

      if fg
        tmp_table.rows.push(row)
      end

      fg = false if row[1].label == to
    }

    return tmp_table
  end

  # 行を任意選択した結果を返す。
  def choice(arr)
    tmp_table = MyLib::MyTemporaryTable.new
    tmp_table.series_collection = self.series_collection
    tmp_table.series_orders = self.series_orders
    tmp_table.series_calculators = self.series_calculators
    tmp_table.col_properties = self.col_properties
    tmp_table.view_options = self.view_options
    self.rows.each{|row|
      if arr.selected?(row[1].label)
        tmp_table.rows.push(row)
      end
    }
    return tmp_table
  end

  def category_axis
    labels = MyLib::MyCategoryAxisLabels.new
    @rows.each{|key, row|
      label = MyLib::MyCategoryAxisLabel.new
      label.text = row.label
      labels.push(label)
    }
    axis = MyLib::MyCategoryAxis.new
    axis.labels = labels
    return axis
  end

end
